<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local acetools - Lib functions file.
 *
 * @package    local_acetools
 * @copyright  2023 LMSACE Dev Team <lmsace.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Add a settings navigation in local acetools plugin.
 *
 * @param $settingsnav settings navigation.
 * @param $context Context.
 */
function local_acetools_extend_settings_navigation($settingsnav, $context) {
    \local_acetools\tools_wrapper::sync_addons('settings_navigation', [$settingsnav, $context]);
}


/**
 * Register the layouts this plugin contains.
 *
 * @return array List of layouts.
 */
function local_acetools_register_addons() {
    return [];
}