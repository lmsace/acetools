<?php


namespace local_acebuilder;

class builder {

    public function generate_builder_content($content, $selectors) {
        $data = [
            'selectors' => json_decode($selectors)
        ];
        
        return $this->rendertemplate('buildercontent', $data);
    }

    public function get_list_elements() {
        $subplugins = \local_acebuilder\plugininfo\aceelement::get_subplugins();
        $data = [];
        foreach ($subplugins as $element) {
            $class = $element->type.'_'.$element->name.'\\'.$element->name;
            if (class_exists($class)) {
                $elementclass = new $class;
                $data['elements'][] = $elementclass->get_thumb_data();
            }
        }   
        return $this->rendertemplate('thumb', $data);
    }

    protected function rendertemplate(string $template, array $data): string {
        global $OUTPUT;
        return $OUTPUT->render_from_template('local_acebuilder/'.$template, $data);
    }
}