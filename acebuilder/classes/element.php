<?php


namespace local_acebuilder;

abstract class element {

    public $name;

    public function __construct() {
        
    }

    abstract public function get_name();

    abstract public function get_title();

    abstract public function get_icon();

    abstract public function get_description();

    public function get_thumb_data() {
        global $OUTPUT;
        $element = 'acelement_'.$this->get_name();
        $data = [
            'name' => $this->get_name(),
            'title' => $this->get_title(),
            'description' => $this->get_description(),
            'icon' => $this->get_icon(),
        ];
        return $data;
        return $OUTPUT->render_from_template($element.'/thumb', $data);
    }

    abstract public function render_view();

    // abstract public functon

}