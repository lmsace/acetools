<?php


require_once('../../config.php');
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_url(new moodle_url('/local/acebuilder/index.php'));

require_once($CFG->dirroot.'/local/acebuilder/lib.php');

echo $OUTPUT->header();
$PAGE->requires->js_call_amd('local_acebuilder/builder', 'init', ['contextid' => $context->id]);
echo html_writer::tag('button', 'Builder', ['class' => 'btn btn-primary showbuilder']);
echo $OUTPUT->footer();