define ("local_acebuilder/builder",["jquery", 'core/modal_factory', 'core/fragment',

], function($, Modal, Fragment, Dragdrop) {

    var self;

    let SELECTORS = {
        addelement: 'builder-add-element',        
    };

    var CSS_SELECTORS = {
        addelement: '.' + SELECTORS.addelement,
    }

    var SELECTORSJSON = JSON.stringify(SELECTORS);

    AceBuilder = function(contextID) {
        self = this;
        this.contextID = contextID;
        this.createBuilderModal();
        this.createElementsModal();
        this.registerEvents();
    };

    AceBuilder.prototype.contextID = 0;

    AceBuilder.prototype.BuilderModal = null;
    AceBuilder.prototype.ElementsModal = null;


    AceBuilder.prototype.registerEvents = function() {
        $('button.showbuilder').click(() => {
            self.showBuilderModal();
        })

        /* self.BuilderModal.getRoot().find(CSS_SELECTORS.addelement).click(() => {
            self.ElementsModal.show();
        }) */
    };

    /**
     * Create Builder content modal.
     */
    AceBuilder.prototype.createBuilderModal = function() {
        Modal.create({
            title: 'Builder Elements', // '{{#str}}builderelements, aceaddon_builder{{/str}}'   
            body: Fragment.loadFragment('local_acebuilder', 'builder_content', self.contextID, {content: '', selectors: SELECTORSJSON}),
            large: true, 
            draggable: true,      
        }).then((modal) => {
            modal.getRoot();
            self.BuilderModal = modal;            

            self.BuilderModal.getRoot().find(CSS_SELECTORS.addelement).click(() => {        
                        
                self.ElementsModal.show();
            }) 
        })
    }

    /**
     * Create Elements List modal.
     */
    AceBuilder.prototype.createElementsModal = function() {
        Modal.create({
            title: 'Builder Elements', // '{{#str}}builderelements, aceaddon_builder{{/str}}'   
            body: Fragment.loadFragment('local_acebuilder', 'get_elements_list', self.contextID),
            large: true,       
        }).then((modal) => {
            self.ElementsModal = modal;
        })
    };

    AceBuilder.prototype.showBuilderModal = function() {
        self.BuilderModal.show();
    };

    AceBuilder.prototype.hideBuilderModal = function() {
        self.BuilderModal.hide();
    };

    AceBuilder.prototype.showElementsModal = function() {
        self.ElementsModal.show();
    };

    AceBuilder.prototype.hideElementsModal = function() {
        self.ElementsModal.hide();
    };

    return {

        init: (contextID) => {
            let builder = new AceBuilder(contextID);
        }
    }
});
//# sourceMappingURL=builder.min.js.map
