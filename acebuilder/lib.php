<?php


function local_acebuilder_load_elements() {
    global $CFG;
    $elementsdir = $CFG->dirroot.'/local/acetools/addons/builder/elements/';
    $files = array_diff(scandir($elementsdir), array('.', '..'));
    $elements = [];
    foreach ($files as $file) {
        $mainfile = $elementsdir.$file.'/'.$file.'.php';
        if (file_exists($mainfile)) {
            require_once($mainfile);
            $elements[] = ('builder_element_'.$file)::load_module();
        }
    }
    return $elements;
}


function local_acebuilder_edit_form($element, $formdata) {
    global $CFG;
    $path = $CFG->dirroot.'/local/acetools/addons/builder/elements/'.$element.'/'.$element.'.php';
    // print_r($element);
    if (file_exists($path)) {
        require_once($path);
        $class = "builder_element_".$element;
        $formclass = new $class();
        print_r(json_decode($formdata));
        $formclass->set_data($formdata);
        return $formclass->render();
    }
}

function local_acebuilder_output_fragment_get_elements_list($args=[]) {
    global $PAGE;
    $contextid = $args['contextid'];
    $context = \context::get_instance_by_id($contextid);
    $PAGE->set_context($context);
    return (new local_acebuilder\builder())->get_list_elements();
    // return $
    
    // return 
}

function local_acebuilder_output_fragment_builder_content($args=[]) {
    $content = $args['content'];
    $selectors = $args['selectors'];
    return (new local_acebuilder\builder())->generate_builder_content($content, $selectors);
}

local_acebuilder_output_fragment_get_elements_list();