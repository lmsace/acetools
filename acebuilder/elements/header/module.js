



function Header() {
    // Header.superclass.constructor.apply(this, arguments);
}

Header.prototype = {


	element_thumb: function() {
		return {id: 'header', icon: 'fa fa-heading', title: 'Heading'};
	},

	element_output: function() {
		return '<div class="heading"><h{{head_level}} class="">{{content}}</h{{head_level}}></div>';
	},

	element_event_register: function() {

	},

	thumb_output: function() {
		return '<div class="header-thumb {{CSS_ATTR.ELEMENTTHUMB}}">' +
			'<div class="img-block">' +
				'<i class="fa fa-heading"></i>' +
			'</div>' +
			'<div class="element-title">' +
				'<span>Heading</span>' +
			'</div>' +
		'</div>';
	},

	form_fields: function() {
		return {
			tabs: [
				{
					name: 'general',
					title: 'General', // M.utill.get_string();
                    class: 'active',
					fields: [
						{
							name: 'head_level',
							title: 'Heading level',
							type: 'select',
							temp: 'SELECT',
							"default": '1',
							options: [
								{value: '1', title: '1'},
								{value: '2', title: '2'},
								{value: '3', title: '3'},
								{value: '4', title: '4'},
								{value: '5', title: '5'},
								{value: '6', title: '6'}
							]
						},
						{
							name: 'class',
							title: 'Custom class',
							type: 'text',
							temp: 'TEXT',
							"default": '',
							placeholder: 'Enter class name to add in heading tag'
						},
						{
							name: 'content',
							title: 'Content',
							type: 'text',
							temp: 'TEXT',
							"default": '',
							placeholder: 'Enter content to add heading'
						}
					]
				}
			]

		};
	}
};

M.aceaddon_builder = M.aceaddon_builder || {};

var AEHeader = M.aceaddon_builder.header = M.aceaddon_builder.header || {};

AEHeader.init = function() {

    return new Header();
};