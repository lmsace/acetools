<?php

namespace aceelement_header;

require_once($CFG->dirroot.'/lib/formslib.php');

class header extends \local_acebuilder\element {

    public function get_name() {
        return 'header';
    }

    public function get_title() {
        return 'Header';
    }

    public function get_icon() {
        return 'fa fa-heading';
    }

    public function get_description() {
        return 'Description content about the header element.';
    }

    public function render_view() {
        global $OUTPUT;
        return $OUTPUT->render_from_template('aceelements_header/header');
    }
}