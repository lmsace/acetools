<?php

namespace aceaddon_builder\elements;

require_once($CFG->dirroot.'/lib/formslib.php');

class header extends \moodleform {

    public function definition() {
        $mform = $this->_form;
        $mform->addElement('select', 'head_level', 'Heading level', array_combine(range(1, 6), range(1, 6)));
        $mform->setType('head_level', PARAM_INT);

        $mform->addElement('text', 'content', 'Content');
        $mform->setType('content', PARAM_TEXT);

        $this->add_action_buttons();
    }

    public static function load_module() {
        global $PAGE;
        $module = array('name'     => 'acebuilder_element_header',
                        'requires' => array('base'),
                        'fullpath' => '/local/acetools/addons/builder/elements/header/module.js');
        
        $PAGE->requires->js_module($module);
        return 'header';
    }
}