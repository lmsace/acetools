<?php

$functions = [
    'aceaddon_courserate_update_ratings' => [
        'classname'     => 'aceaddon_courserate\external',
        'classpath'     => '',
        'methodname'    => 'update_ratings',
        'description'   => 'Update user course ratings and reviews',
        'type'          => 'read',
        'ajax'          => true,
        'loginrequired' => false,
    ]
];