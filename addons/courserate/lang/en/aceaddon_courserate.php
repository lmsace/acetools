<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Aceaddon courserate - Language strings file.
 *
 * @package    aceaddon_courserate
 * @copyright  2023 LMSACE Dev Team <lmsace.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Course rate - LMSACE Tools Addon';
$string['leave_rating'] = 'Leave Rating';

$string['ratetext1'] = 'Rate TEXT 1';
$string['ratetext2'] = 'Rate TEXT 2';
$string['ratetext3'] = 'Rate TEXT 3';
$string['ratetext4'] = 'Rate TEXT 4';
$string['ratetext5'] = 'Rate TEXT 5';

$string['reviews'] = 'Reviews';
$string['courserating'] = 'Course Ratings';
$string['seemorereviews'] = 'See more reviews';
$string['coursereview'] = 'Course Review';
$string['seemorereviews'] = 'See more reviews';
$string['saveandcontinue'] = 'Save and continue';
$string['ratingempty'] = 'Rating is empty';
$string['coursereview'] = 'Course review';
$string['ratecourse'] = 'Rate course';
$string['avaragerating'] = 'Avarage Rating';
$string['notview'] = 'Edit this block\'s settings to finish configuration';
$string['giverate'] = 'Give a rate';
$string['courserating_help'] = 'Give a ratings for this course';