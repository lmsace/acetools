define(['jquery', 'core/str', 'core/modal_factory', 'core/modal_events', 'core/templates', "aceaddon_courserate/rater", 'core/ajax', 'core/fragment'],
 function($, Str, Modal, ModalEvents, Templates, raterJs, AJAX, Fragment) {
    'use strict';

    var SELECTORS = {
        rating: '#courserate_trigger',
        ratingText: '#courserate_description',
        avgRater: '#average-rating',
        reviewFormID: "courserate-review"
    };

    var isRated = false,
isReviewEventRegistered = false;

    var THIS, rateStrings, rateModal;

    var Rating = function(courseid, userRating, userReview) {
        this.courseid = courseid;
        this.userRating = userRating;
        this.userReview = userReview;
        this.setupModal();
        this.loadRateText();
        THIS = this;
    };

    Rating.prototype.courseid = 0;

    Rating.prototype.userRating = 0;

    Rating.prototype.userReview = '';

    Rating.prototype.loadRateText = function() {
        Str.get_strings([
            {key: 'ratetext1', component: 'aceaddon_courserate'},
            {key: 'ratetext2', component: 'aceaddon_courserate'},
            {key: 'ratetext3', component: 'aceaddon_courserate'},
            {key: 'ratetext4', component: 'aceaddon_courserate'},
            {key: 'ratetext5', component: 'aceaddon_courserate'},
        ]).done(strings => {
            rateStrings = strings;
        });
    };

    Rating.prototype.setupModal = function() {

        Modal.create({
            // Type: Modal.types.SAVE_CANCEL,
            title: Str.get_string('ratecourse', 'aceaddon_courserate'),
            body: Templates.render('aceaddon_courserate/ratingmodal', {leaverating: true, rating: this.userRating, review: this.userReview}),
        }, $(SELECTORS.rating))
        .done(modal => {
            rateModal = modal;
            modal.getRoot().on(ModalEvents.shown, function() {
                THIS.setRating();
                if (!isReviewEventRegistered) {
                    document.querySelector('form#' + SELECTORS.reviewFormID).addEventListener('submit', function(e) {
                        e.preventDefault();
                        isReviewEventRegistered = true;
                        var formdata = new FormData(document.forms[SELECTORS.reviewFormID]);
                        var review = formdata.get('coursereview');
                        THIS.updateReview(review);
                    });
                }
            });
        });

    };

    Rating.prototype.showReviewSection = function() {
        document.querySelector('form#' + SELECTORS.reviewFormID).classList.remove('hide');
    };

    Rating.prototype.setRating = function() {
        if (!isRated) {
            var myRating = raterJs({
                element: document.querySelector("#rater"),
                // ShowToolTip: true,
                onHover: function(currentRating, rate) {
                    THIS.updateRatingText(currentRating, rate);
                },
                onRating: function(rating) {
                    isRated = true;
                    THIS.showReviewSection();
                },
                rateCallback: function rateCallback(rating, done) {
                    this.setRating(rating);
                    done();
                    isRated = true;
                    THIS.updateRating(rating);
                }
            });
        }
    };

    Rating.prototype.updateRatingText = function(currentRating, rate) {
        document.querySelector(SELECTORS.ratingText).innerHTML = (rateStrings[currentRating - 1] != undefined) ? rateStrings[currentRating - 1] : '';
    };

    Rating.prototype.updateRating = function(rating) {

        var promises = AJAX.call([{
            methodname: 'aceaddon_courserate_update_ratings',
            args: {courseid: THIS.courseid, rating: rating}
        }]);

        promises[0].done((response) => {

        });
    };

    Rating.prototype.updateReview = function(review) {

        var promises = AJAX.call([{
            methodname: 'aceaddon_courserate_update_ratings',
            args: {courseid: THIS.courseid, rating: null, review: review}
        }]);

        promises[0].done((response) => {
            rateModal.hide();
        });
    };

    return {

        init: function(courseid, userRating = null, userReview = '') {
            new Rating(courseid, userRating, userReview);
        },

        courserate_widget: function() {
            // Average Rating.
            if (document.querySelector(SELECTORS.avgRater) != undefined) {
                raterJs({
                    element: document.querySelector(SELECTORS.avgRater),
                    readOnly: true,
                    rateCallback: function rateCallback(rating, done) {
                        this.setRating(rating);
                        done();
                    }
                });
            }
        },

        rating_widget: function(courseid) {

            var widgetStars = '.courseraing-ratingwidget .star-rating';
            document.querySelectorAll(widgetStars).forEach(element => {
                raterJs({
                    element: element,
                    readOnly: true,
                    rateCallback: function rateCallback(rating, done) {
                        this.setRating(rating);
                        done();
                    },
                });
            });
        },

        review_widget: function(contextid, courseid, start = 1, limit = 10) {

            var widgetStars = '.courseraing-reviewwidget .user-rating';
            var moreReviews = '#load-more-reviews';

            if (document.querySelectorAll(widgetStars) !== null) {
                document.querySelectorAll(widgetStars).forEach(element => {
                    raterJs({
                        element: element,
                        readOnly: true,
                        rateCallback: function rateCallback(rating, done) {
                            this.setRating(rating);
                            done();
                        },
                    });
                });
            }

            if (document.querySelector(moreReviews) !== null) {
                document.querySelector(moreReviews).addEventListener('click', function() {
                    var newstart = start + limit;
                    var params = {courseid: courseid, start: newstart};
                    Fragment.loadFragment('aceaddon_courserate', 'load_morereviews', contextid, params).done((html, js) => {
                        if (html) {
                            start = newstart;
                        }
                        Templates.appendNodeContents('.courseraing-reviewwidget .review-content', html, js);
                    });
                });
            }
        }
    };
});

