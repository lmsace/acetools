<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Aceaddon courserate - Courser rating class file.
 *
 * @package    aceaddon_courserate
 * @copyright  2023 LMSACE Dev Team <lmsace.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace aceaddon_courserate;

use html_writer;
use moodle_exception;
use stdclass;
use moodle_url;

class rating {

    public $review = '';
    public $rating;
    public $user;
    public $allratings = [];

    public function __construct(int $courseid=null, $rating=null, $review='', $userid=null) {
        global $PAGE, $USER;
        $this->course = (object) (($courseid) ? get_course($courseid) : $PAGE->course);
        $this->rating = $rating;
        $this->review = $review;
        $this->userid = ($userid) ?: $USER->id;
        $avgratings = $this->get_averagerating  ();
        // print_r($avgratings);exit;
    }

    /**
     * Update the current user ratings in ratings table.
     *
     * @param float $rating rating value.
     * @param string $review Review value.
     *
     * @return blool
     */
    public function update_currentuser_rating(float $rating=null, string $review='') {
        global $USER;
        if (empty($rating) && empty($review)) {
            throw new moodle_exception('ratingempty', 'aceaddon_courserate');
        }
        $this->userid = $USER->id;
        return $this->update_user_rating();
    }

    /**
     * Get all available ratings in the rating table.
     */
    public function get_availableratings() {
        global $DB;
        $records = $DB->get_records('aceaddon_rating', ['courseid' => $this->course->id]);
        $this->allratings = $records;
        return $records;
    }

    /**
     * Get the avarage ratings in the acceddon rating table.
     */
    public function get_averagerating() {
        global $DB;
        if (empty($this->allratings)) {
           $records = $this->get_availableratings();
            if (empty($records)) {
                return 0;
            }
        }
        $ratings = array_sum(array_column($this->allratings, 'rating'));
        return ($ratings/count($this->allratings));
    }

    /**
     * Get the user id based ratings.
     *
     * @param int $userid User id.
     */
    public function get_userrating($userid) {
        global $DB;
        return $DB->get_record('aceaddon_rating', ['courseid' => $this->course->id, 'userid' => $userid]);
    }


    /**
     * Update the ratings in the rating table.
     */
    public function update_user_rating() {
        global $DB;
        if (empty($this->rating) && empty($this->review)) {
            throw new moodle_exception('ratingempty', 'aceaddon_courserate');
        }

        if ($record = $this->get_userrating($this->userid)) {
            if ($this->rating) {
                $record->rating = $this->rating;
            }
            if ($this->review) {
                $record->review = $this->review;
            }
            return $DB->update_record('aceaddon_rating', $record);
        } else {
            $record = new stdclass();
            $record->courseid = $this->course->id;
            $record->userid = $this->userid;
            if ($this->rating) {
                $record->rating = $this->rating;
            }
            if ($this->review) {
                $record->review = $this->review;
            }
            $record->timecreated = time();
            $record->timemodified = time();
            return $DB->insert_record('aceaddon_rating', (array) $record);
        }
    }

    /**
     * Add a review link in course page.
     *
     * @return string.
     */
    public function add_review_link() {

        // $this->rating_widget();

        global $PAGE, $USER, $OUTPUT;
        $rating = $this->get_userrating($USER->id);
        $rate = (isset($rating->rating)) ? $rating->rating : 0;
        $review = (isset($rating->review)) ? $rating->review : '';
        $PAGE->requires->js_call_amd('aceaddon_courserate/ratings', 'init',
            ['courseid' => $this->course->id, 'userRating' => $rate, 'userReview' => $review]
        );
        return $OUTPUT->render_from_template('aceaddon_courserate/ratingmodal', ['courserate' => 1]);
    }

    /**
     * Get a course rate widget in course page.
     *
     * @return string.
     */
    public function courserate_widget() {
        global $OUTPUT, $PAGE;
        $avgrating = $this->get_averagerating();
        $params = ['courserate' => 1, 'avgrating' => $avgrating];
        $PAGE->requires->js_call_amd('aceaddon_courserate/ratings', 'courserate_widget', ['courseid' => $this->course->id]);
        return $OUTPUT->render_from_template('aceaddon_courserate/ratingwidget', $params);
    }

    /**
     * Display the rating widget course page.
     *
     * @return string.
     */
    public function rating_widget() {
        global $OUTPUT, $PAGE;
        $data = new stdclass();
        $data->averagerating = $this->get_averagerating();
        $ratingslist = $this->allratings; // array_column($this->allratings, 'ratings');
        $bars = [];
        foreach (range(1, 5) as $rate) {
            $list = array_filter($ratingslist, function($rating) use ($rate) {
                return ($rating->rating == $rate);
            });

            $width = (!empty($list)) ? (count($list) / count($ratingslist)) * 100 : "0";

            $bars[] = [
                'n' => $rate,
                'width' => $width.'%',
            ];
        }
        $data->bars = array_reverse($bars);
        $data->ratingsview = 1;
        $PAGE->requires->js_call_amd('aceaddon_courserate/ratings', 'rating_widget', ['courseid' => $this->course->id]);

        return $OUTPUT->render_from_template('aceaddon_courserate/ratingwidget', $data);
    }

    /**
     * Display the review widget in the course page.
     *
     * @param int $start.
     * @param int $limit.
     * @param bool $itemonly.
     *
     * @return string.
     */
    public function review_widget($start=0, $limit=10, $itemonly=false) {
        global $DB, $PAGE, $OUTPUT;
        $reviews = [];
        $ratings = $DB->get_records_sql('SELECT * FROM {aceaddon_rating} WHERE courseid=:courseid or review <> "" ', ['courseid' => $this->course->id], $start, $limit);
        if (empty($ratings)) {
            return null;
        }
        foreach ($ratings as $key => $rate) {
            $user = \core_user::get_user($rate->userid);
            if ($user) {
                $username = fullname($user);
                $userprofileurl = new moodle_url('/user/profile.php', ['id' => $user->id]);
                $userpicture = new \user_picture($user);
                $userpicture->size = 1; // Size f1.
                $reviews[] = [
                    'profileimageurl' => $userpicture->get_url($PAGE)->out(false),
                    'username' => $username,
                    'userprofileurl' => $userprofileurl,
                    'rating' => $rate->rating,
                    'review' => $rate->review,
                ];
            }
        }
        $data = new stdclass();
        $data->reviewsview = true;
        $data->reviews = $reviews;
        $data->itemonly = $itemonly;
        $context = \context_course::instance($this->course->id);
        if (!$itemonly) {
            $PAGE->requires->js_call_amd('aceaddon_courserate/ratings', 'review_widget', ['contextid' => $context->id, 'courseid' => $this->course->id]);
        }
        return $OUTPUT->render_from_template('aceaddon_courserate/ratingwidget', $data);
    }
}