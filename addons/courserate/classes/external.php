<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Aceaddon courserate - External class file for sub plugin course rate.
 *
 * @package    aceaddon_courserate
 * @copyright  2023 LMSACE Dev Team <lmsace.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace aceaddon_courserate;

use external_function_parameters;
use external_multiple_structure;
use external_single_structure;
use external_value;
use moodle_exception;

/**
 * External function for update course ratings.
 */
class external extends \external_api {

    public static function update_ratings_parameters() {
        return new external_function_parameters(
            array(
                'courseid' => new external_value(PARAM_INT, 'Course ID to update ratings'),
                'rating' => new external_value(PARAM_FLOAT, 'Rating number', VALUE_OPTIONAL, 0),
                'review' => new external_value(PARAM_RAW, 'Review for the course', VALUE_OPTIONAL, '')
            )
        );
    }

    public static function update_ratings(int $courseid, float $rating=null, string $review=''): string {
        global $PAGE;
        $coursecontext = \context_course::instance($courseid);
        $PAGE->set_context($coursecontext);
        $params = self::validate_parameters(self::update_ratings_parameters(), array('courseid' => $courseid, 'rating' => $rating, 'review' => $review));

        if (empty($params['rating']) && empty($params['review'])) {
            throw new moodle_exception('ratingandreviewshouldgiven', 'aceaddon_courserate');
        }
        $rater = new rating($params['courseid'], $params['rating'], $params['review']);
        return $rater->update_user_rating();
    }

    public static function update_ratings_returns() {
        return new external_value(PARAM_TEXT, 'Result of score updated or not');
    }
}