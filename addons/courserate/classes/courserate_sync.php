<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Aceaddon courserate - Define course rate sync class file.
 *
 * @package    aceaddon_courserate
 * @copyright  2023 LMSACE Dev Team <lmsace.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace aceaddon_courserate;

class courserate_sync extends \local_acetools\tools_wrapper {

    public static function page_init($page) {
    }

    /**
     * Added a review link button in settings navigation in the course view page.
     */
    public static function settings_navigation() {
        global $PAGE;
        if (strpos($PAGE->pagetype, 'course-view') !== false) {
         /* $reviewlink = (new \aceaddon_courserate\rating())->add_review_link();
         $PAGE->set_button($reviewlink); */
        }
    }
}
