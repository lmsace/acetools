<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Lib functions for lmsace quicklinks.
 *
 * @package    aceaddon_quicklinks
 * @copyright  2023 LMSACE Dev Team <lmsace.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use core_user\output\myprofile\tree;

/**
 * Defines lmsace quicklinks nodes for my profile navigation tree.
 *
 * @param \core_user\output\myprofile\tree $tree Tree object
 * @param stdClass $user user object
 * @param bool $iscurrentuser is the user viewing profile, current user ?
 * @param stdClass $course course object
 *
 * @return bool
 */
function aceaddon_quicklinks_myprofile_navigation(tree $tree, $user, $iscurrentuser, $course) {
    global $USER;

    // Get the quicklinks category.
    if (!array_key_exists('aceaddon_quicklinks', $tree->__get('categories'))) {
        // Create the category.
        $categoryname = get_string('configtitle', 'aceaddon_quicklinks');
        $category = new core_user\output\myprofile\category('aceaddon_quicklinks', $categoryname, 'privacyandpolicies');
        $tree->add_category($category);
    } else {
        // Get the existing category.
        $category = $tree->__get('categories')['aceaddon_quicklinks'];
    }

    if ($iscurrentuser) {
        $createquicklinkurl = new moodle_url('/local/acetools/addons/quicklinks/manage.php', array('userid' => $USER->id));
        $quicklinksnode = new core_user\output\myprofile\node('aceaddon_quicklinks', 'aceaddon_quicklinks',
            get_string('quicklinks', 'aceaddon_quicklinks'), null, $createquicklinkurl);
        $tree->add_node($quicklinksnode);
    }

}

/**
 * Serve the files from the Pulse file areas
 *
 * @param stdClass $course the course object
 * @param stdClass $cm the course module object
 * @param stdClass $context the context
 * @param string $filearea the name of the file area
 * @param array $args extra arguments (itemid, path)
 * @param bool $forcedownload whether or not force download
 * @param array $options additional options affecting the file serving
 * @return bool false if the file not found, just send the file otherwise and do not return anything
 */
function aceaddon_quicklinks_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()) {
    // Check the contextlevel is as expected - if your plugin is a block, this becomes CONTEXT_BLOCK, etc.
    if ($context->contextlevel != CONTEXT_MODULE && $context->contextlevel != CONTEXT_SYSTEM) {
        return false;
    }
    // Get extended plugins files.
    $files = ['linkimage'];
    // Make sure the filearea is one of those used by the plugin.
    if (!in_array($filearea, $files)) {
        return false;
    }

    // Item id is 0.
    $itemid = array_shift($args);

    // Extract the filename / filepath from the $args array.
    $filename = array_pop($args); // The last item in the $args array.
    if (!$args) {
        $filepath = '/'; // ...$args is empty => the path is '/'
    } else {
        $filepath = '/'.implode('/', $args).'/'; // ...$args contains elements of the filepath
    }

    // Retrieve the file from the Files API.
    $fs = get_file_storage();
    $file = $fs->get_file($context->id, 'aceaddon_quicklinks', $filearea, $itemid, $filepath, $filename);
    if (!$file) {
        return false; // The file does not exist.
    }

    // We can now send the file back to the browser - in this case with a cache lifetime of 1 day and no filtering.
    send_stored_file($file, 86400, 0, $forcedownload, $options);
}

/**
 *
 */
function aceaddon_quicklinks_register_addons() : array {
    return [
        [
            'name' => get_string('quicklinks', 'aceaddon_quicklinks'),
            'class' => aceaddon_quicklinks\quicklinks_widget::class,
        ]
    ];
}