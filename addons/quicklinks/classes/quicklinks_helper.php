<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Helper class for lmsace quicklinks.
 *
 * @package    aceaddon_quicklinks
 * @copyright  2023 LMSACE Dev Team <lmsace.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


namespace aceaddon_quicklinks;

use context_system;
use stdClass;
use moodle_url;
use single_button;
use moodle_exception;

require_once($CFG->libdir . '/filelib.php');

class quicklinks_helper {

    /**
     * Insert or update the quicklinks instance to DB.
     *
     * @param object $formdata form data from quicklinks form
     * @param int $userid Users id.
     * @return $record.
     */
    public function manage_instance($formdata, $userid) {
        global $DB, $OUTPUT;

        $record = $formdata;
        $record->userid = $userid;
        $record->linkimage = $formdata->linkimage_filemanager;
        $record->icon = $formdata->icon;
        $transaction = $DB->start_delegated_transaction();

        if (isset($formdata->id) && $DB->record_exists('aceaddon_quicklinks', ['id' => $formdata->id])) {
            $record->id = $formdata->id;

            $DB->update_record('aceaddon_quicklinks', $record);
            self::postupdate_filemanager_files($record);
            // Show the edited success notification.
            \core\notification::success(get_string('updatesuccess', 'aceaddon_quicklinks'));
        } else {

            $record->id = $DB->insert_record('aceaddon_quicklinks', $record);
            self::postupdate_filemanager_files($record);
            \core\notification::success(get_string('insertsuccess', 'aceaddon_quicklinks'));
        }

        // Allow to update the DB changes to Database.
        $transaction->allow_commit();

        return $record->id;

    }


    /**
     * Generate the button which is displayed on top of the manage quicklinkss table. Helps to create quicklinkss.
     *
     * @return string The HTML contents to display the create quicklinkss button.
     */
    public function create_quicklinks_button() {
        global $OUTPUT;

        // Setup create quicklinks button on page.
        $caption = get_string('createquicklinks', 'aceaddon_quicklinks');
        $editurl = new moodle_url('/local/acetools/addons/quicklinks/edit.php', ['sesskey' => sesskey()]);

        // IN Moodle 4.2, primary button param depreceted.
        $button = new single_button($editurl, $caption, 'get');
        $button = $OUTPUT->render($button);
        return $button;
    }

     /**
     * Fetches a quicklinks record from the database by ID and return values.
     *
     * @param int $id The ID of the quicklinks to fetch.
     * @return stdClass|false Returns an object if the quicklinks is found, false otherwise.
     */
    public static function get_quicklinks($id) {
        global $DB, $OUTPUT;

        // Verfiy and Fetch quicklinks record from DB.
        if ($record = $DB->get_record('aceaddon_quicklinks', ['id' => $id])) {
            $record->title = $record->title;
            $record->userid	 =  $record->userid;
            $record->link = $record->link;
            $record->icon = $record->icon;
            $record->linkimage = $record->linkimage ?: "";
            return $record;
        } else {
            // TODO: string for quicklinks not found.
            throw new moodle_exception('quicklinknotfound', 'aceaddon_quicklinks');
        }
        return false;
    }

    /**
     * Quicklinks form editor element options.
     *
     * @return array
     */
    public static function get_filemanager_options() {
        global $CFG;
        return array(
            'maxfiles' => 1,
            'maxbytes' => $CFG->maxbytes,
            'context' => \context_system::instance(),
            'noclean' => true
        );
    }

    /**
     * Postupdate the filemanager files.
     */
    public static function postupdate_filemanager_files($quicklinksdata) {
        global $DB;
        $itemid = isset($quicklinksdata->id) ? $quicklinksdata->id : null;
        $upd = new stdClass();
        $upd->id = $quicklinksdata->id;
        $quicklinksdata = file_postupdate_standard_filemanager($quicklinksdata, 'linkimage', self::get_filemanager_options(),
            context_system::instance(), 'aceaddon_quicklinks', 'linkimage', $itemid);
        $upd->linkimage = $quicklinksdata->linkimage;
        $DB->update_record('aceaddon_quicklinks', $upd);
    }

    /**
     * Loads the prepare editor files.
     *
     * @param object $quicklinks fetch the quicklinks data.
     */
    public static function prepare_filemanger_files($quicklinks) {
        $itemid = isset($quicklinks->id) ? $quicklinks->id : null;
        $quicklinks = file_prepare_standard_filemanager($quicklinks, 'linkimage', self::get_filemanager_options(),
            context_system::instance(), 'aceaddon_quicklinks', 'linkimage', $itemid);
        return $quicklinks;
    }

     /**
     * Create an instance of the quicklinks class from the given quicklinks ID or quicklinks object/array.
     *
     * @param int|stdclass $quicklinks
     * @return quicklinks
     */
    public static function instance($quicklinks) {

        if (is_scalar($quicklinks)) {
            $quicklinks = self::get_quicklinks($quicklinks);
        }

        if (!is_array($quicklinks) && !is_object($quicklinks)) {
            throw new moodle_exception('quicklinksformatnotcorrect', 'local_magic');
        }
        return new self($quicklinks);
    }

    /**
     * Delete the current quicklinks and all its associated items from the database.
     *
     * @return bool True if the deletion is successful, false otherwise.
     */
    public function delete_quicklinks($id) {
        global $DB;
        if ($DB->delete_records('aceaddon_quicklinks', ['id' => $id])) {
            return true;
        }
        return false;
    }

    /**
     * Get the quick link image.
     *
     * @param object $record quick links record.
     * @return array $image image data.
     */
    public function get_linkimage($record) {
        $image = [];
        $fs = get_file_storage();
        $contextid = \context_system::instance()->id;
        $files = $fs->get_area_files($contextid, 'aceaddon_quicklinks', 'linkimage', $record->id, '', false);
        $i = 0;
        foreach ($files as $file) {
            if (!empty($files)) {
                // Get the first file.
                $file = reset($files);

                // Conver the file to url.
                $image['linkimage'] = \moodle_url::make_pluginfile_url(
                    $file->get_contextid(),
                    $file->get_component(),
                    $file->get_filearea(),
                    $file->get_itemid(),
                    $file->get_filepath(),
                    $file->get_filename(),
                    false
                );
            }
        }
        return $image;
    }
 }
