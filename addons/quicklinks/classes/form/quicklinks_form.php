<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Quick links form class information.
 *
 * @package    aceaddon_quicklinks
 * @copyright  2023 LMSACE Dev Team <lmsace.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace aceaddon_quicklinks\form;
use aceaddon_quicklinks;

use core\plugininfo\local;
use tool_brickfield\local\areas\mod_choice\option;

require_once($CFG->dirroot.'/lib/formslib.php');

/**
 * Create new quicklinks form.
 *
 * @copyright  2023 LMSACE Dev Team <lmsace.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class quicklinks_form extends \moodleform {

    public function definition() {
        global $USER, $CFG, $COURSE, $PAGE, $DB, $OUTPUT;
        $mform = $this->_form;

        $mform->addElement('hidden', 'id', 0);
        $mform->setType('id', PARAM_INT);

        // Add the Title field (required).
        $mform->addElement('text', 'title', get_string('title', 'aceaddon_quicklinks'));
        $mform->setType('title', PARAM_TEXT);
        $mform->addRule('title', get_string('error'), 'required');

        // Add linkimage as filemanager element.
        $helper = new aceaddon_quicklinks\quicklinks_helper();
        $options = $helper->get_filemanager_options();
        $mform->addElement('filemanager', 'linkimage_filemanager', get_string('linkimage', 'aceaddon_quicklinks'), null, $options);

        // Add quick Link.
        $mform->addElement('text', 'link', get_string('link', 'aceaddon_quicklinks'));
        $mform->setType('link', PARAM_RAW);
        $mform->addRule('link', get_string('error'), 'required');

        //Quick links Icon.
        $theme = \theme_config::load($PAGE->theme->name);
        $faiconsystem = \core\output\icon_system_fontawesome::instance($theme->get_icon_system());
        $iconlist = $faiconsystem->get_core_icon_map();
        array_unshift($iconlist, '');
        $mform->addElement('autocomplete', 'icon', get_string('quicklinkicon', 'aceaddon_quicklinks'), $iconlist);
        $mform->setType('icon', PARAM_TEXT);

        $this->add_action_buttons();
    }


}