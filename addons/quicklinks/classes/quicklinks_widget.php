<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Additional Quick links.
 *
 * @package    aceaddon_quicklinks
 * @copyright  2023 LMSACE Dev Team <lmsace.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace aceaddon_quicklinks;
use moodle_url;
 class quicklinks_widget {

    /**
     * Build on the addon quicklinks.
     */
    public function build() {
        global $DB, $USER, $OUTPUT;
        $data = [];
        if ($DB->record_exists('aceaddon_quicklinks', ['userid' => $USER->id])){
            $records = $DB->get_records('aceaddon_quicklinks', ['userid' => $USER->id]);
            foreach ($records as $record) {
                $icon = explode(':', $record->icon);
                $icon1 = isset($icon[1]) ? $icon[1] : 'core';
                $icon0 = isset($icon[0]) ? $icon[0] : '';
                $linkicon = $OUTPUT->pix_icon($icon1, $icon0);
                $helper = new \aceaddon_quicklinks\quicklinks_helper();
                $addlinkbtn = new moodle_url('/local/acetools/addons/quicklinks/edit.php', ['sesskey' => sesskey()]);
                $data = [
                    'title' => $record->title,
                    'links' => $record->link,
                    'icon' => $linkicon,
                    'image' => $helper->get_linkimage($record),
                ];
                $template['data'][] = $data;
            }
            $template['btnurl'] = $addlinkbtn;
            return $OUTPUT->render_from_template('aceaddon_quicklinks/displayquicklinks', $template);
        }
    }
 }
