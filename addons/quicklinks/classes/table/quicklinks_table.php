<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *  Table that lists quicklinks.
 *
 * @package    aceaddon_quicklinks
 * @copyright  2023 LMSACE Dev Team <lmsace.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace aceaddon_quicklinks\table;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/tablelib.php');
require_once($CFG->dirroot . '/local/acetools/addons/quicklinks/lib.php');

use moodle_url;
use html_writer;

/**
 * Table that lists quicklinks.
 *
 * @package aceaddon_quicklinks
 */
class quicklinks_table extends \table_sql {

    /**
     * Setup and Render the quicklinks table.
     *
     * @param int $pagesize Size of page for paginated displayed table.
     * @param bool $useinitialsbar Whether to use the initials bar which will only be used if there is a fullname column defined.
     * @param string $downloadhelpbutton
     */
    public function out($pagesize, $useinitialsbar, $downloadhelpbutton = '') {

        // Define table headers and columns.
        $columns = ['title', 'link', 'icon', 'action'];
        $headers = [
            get_string('title', 'aceaddon_quicklinks'),
            get_string('link', 'aceaddon_quicklinks'),
            get_string('icon', 'aceaddon_quicklinks'),
            get_string('action'),
        ];

        $this->define_columns($columns);
        $this->define_headers($headers);

        // Remove sorting for some fields.
        $this->sortable(false);

        $this->guess_base_url();

        parent::out($pagesize, $useinitialsbar, $downloadhelpbutton);
    }

    /**
     * Guess the base url for the campaign items table.
     */
    public function guess_base_url(): void {
        global $USER;
        $this->baseurl = new moodle_url('/local/acetools/addons/quicklinks/manage.php', ['userid' => $USER->id]);
    }

    /**
     * Set the sql query to fetch quicklinks list.
     *
     * @param int $pagesize Size of page for paginated displayed table.
     * @param boolean $useinitialsbar Whether to use the initials bar which will only be used if there is a fullname column defined.
     * @return void
     */
    public function query_db($pagesize, $useinitialsbar = true) {
        // Fetch all avialable records from aceaddon_quicklinks table.
        global $USER;
        $select = "*";
        $from = "{aceaddon_quicklinks}";
        $where = 'userid = :userid';
        $param = ['userid' => $USER->id];
        $this->set_sql($select, $from, $where, $param);

        parent::query_db($pagesize, $useinitialsbar);
    }

    /**
     * Show the campaign title in the list.
     *
     * @param object $row
     * @return void
     */
    public function col_title($row) {
        global $OUTPUT;
        $title = html_writer::tag('h6', $row->title, ['class' => 'badge-title']);
        return $title;
    }

    public function col_icon($row) {
        $icon = html_writer::tag('h6', $row->icon, ['class' => 'alert alert-icon']);
        return $icon;
    }

    /**
     * Actions Column, which contains the options to update the campaign visibility, Update the campaign, quicklinks link, search.
     *
     * @param  \stdclass $row
     * @return string
     */
    public function col_action($row) {
        global $OUTPUT;

        $baseurl = new \moodle_url('/local/acetools/addons/quicklinks/manage.php', [
            'id' => $row->id,
            'sesskey' => \sesskey()
        ]);
        $actions = array();

        // Edit.
        $actions[] = array(
            'url' => new moodle_url('/local/acetools/addons/quicklinks/edit.php', [
                'id' => $row->id,
                'sesskey' => sesskey()
            ]),
            'icon' => new \pix_icon('t/edit', \get_string('edit')),
            'attributes' => array('class' => 'action-edit')
        );

        // Delete.
        $actions[] = array(
            'url' => new \moodle_url($baseurl, array('action' => 'delete')),
            'icon' => new \pix_icon('t/delete', \get_string('delete')),
            'attributes' => array('class' => 'action-delete'),
            'action' => new \confirm_action(get_string('deleteconfirm', 'aceaddon_quicklinks'))
        );

        $actionshtml = array();
        foreach ($actions as $action) {
            $action['attributes']['role'] = 'button';
            $actionshtml[] = $OUTPUT->action_icon(
                $action['url'],
                $action['icon'],
                ($action['action'] ?? null),
                $action['attributes']
            );
        }
        return html_writer::span(join('', $actionshtml), 'quicklinks-actions item-actions mr-0');;
    }
}
