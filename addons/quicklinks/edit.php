<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Quick links edit page.
 *
 * @package    aceaddon_quicklinks
 * @copyright  2023 LMSACE Dev Team <lmsace.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use tool_brickfield\local\areas\mod_choice\option;

require_once('../../../../config.php');
require_login();
require_once($CFG->libdir.'/adminlib.php');

require_sesskey();
 // Set external page admin.
$context = context_system::instance();

$id = optional_param('id', null, PARAM_INT);
// Page URL.
$url = new moodle_url($CFG->wwwroot."/local/acetools/addons/quicklinks/edit.php", ['sesskey' => sesskey()]);

// Prepare filearea for quicklinks records.
if ($id) {
    $helper = new aceaddon_quicklinks\quicklinks_helper();
    $quicklinks = $helper->get_quicklinks($id);
    $quicklinks->id = $id;
    $quicklinks = $helper->prepare_filemanger_files($quicklinks);
} else {
    $quicklinks = new stdClass();
    $helper = new aceaddon_quicklinks\quicklinks_helper();
    $quicklinks = $helper->prepare_filemanger_files($quicklinks);
}

if (is_siteadmin()) {
    $PAGE->set_pagelayout('admin');
}

// Setup page values.
$strcreatequicklinks = get_string('createquicklinks', 'aceaddon_quicklinks');
$PAGE->set_url($url);
$PAGE->set_context($context);
$PAGE->set_title("$SITE->fullname: ". $strcreatequicklinks);
$PAGE->set_heading($SITE->fullname);

$quicklinksform = new \aceaddon_quicklinks\form\quicklinks_form();

// Process the submitted items form data.
$returnurl = new moodle_url('./manage.php', ['userid' => $USER->id]);

if ($formdata = $quicklinksform->get_data()) {

    $helper = new aceaddon_quicklinks\quicklinks_helper();
    $result = $helper->manage_instance($formdata, $USER->id);
    if ($result) {
        redirect($returnurl);
    }

} else if ($quicklinksform->is_cancelled()) {
    redirect($returnurl);
}

// Setup the quicklinks to the form, if the form id param available.
if ($id !== null && $id > 0) {
    $quicklinksform->set_data($quicklinks);
} else {
    $quicklinksform->set_data($quicklinks);
}

// Page content display started.
echo $OUTPUT->header();

// quicklinks heading.
echo $OUTPUT->heading(get_string('createquicklinks', 'aceaddon_quicklinks'));

// Display the quicklinkss form for create or edit.
echo $quicklinksform->display();

// Footer.
echo $OUTPUT->footer();
