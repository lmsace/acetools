<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Display list of quicklinks.
 *
 * @package    aceaddon_quicklinks
 * @copyright  2023 LMSACE Dev Team <lmsace.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use tool_brickfield\local\areas\mod_choice\option;

require_once('../../../../config.php');
require_login();
require_once($CFG->libdir.'/adminlib.php');

$action = optional_param('action', null, PARAM_ALPHAEXT);

// Set external page admin.
$context = context_system::instance();

// Page URL.
$url = new moodle_url($CFG->wwwroot."/local/acetools/addons/quicklinks/manage.php");

// Setup page values.
$strcreatequicklinks = get_string('quicklinks', 'aceaddon_quicklinks');
$PAGE->set_url($url);
$PAGE->set_context($context);
$PAGE->set_title("$SITE->fullname: ". $strcreatequicklinks);
$PAGE->set_heading($SITE->fullname);

// Verfiy the user session.
if ($action !== null && confirm_sesskey()) {
    // Every action is based on quicklinks, quicklinks id param should exist.
    $id = required_param('id', PARAM_INT);
    // Create quicklinks instance. Actions are performed in quicklinks instance.
    $helper = new \aceaddon_quicklinks\quicklinks_helper();
    $quicklinks = $helper->instance($id);

    $transaction = $DB->start_delegated_transaction();
    // Perform the requested action.
    switch ($action) :
        // Triggered action is delete, then init the deletion of quicklinks.
        case 'delete':
            // Delete the quicklinks.
            if ($quicklinks->delete_quicklinks($id)) {
                // Notification to user for quicklinks deleted success.
                \core\notification::success(get_string('quicklinksdeleted', 'aceaddon_quicklinks'));
            }
            break;
   endswitch;
    // Allow to update the changes to database.
    $transaction->allow_commit();
    // End of any action redirect to overview page for clear the params from url.
    redirect($url);
}

$table = new aceaddon_quicklinks\table\quicklinks_table($context->id);
$table->define_baseurl($PAGE->url);

// Page content display started.
echo $OUTPUT->header();

// quicklinks heading.
echo $OUTPUT->heading(get_string('quicklinks', 'aceaddon_quicklinks'));

$result = new \aceaddon_quicklinks\quicklinks_helper();
//echo $result->build_addon_quicklinks();

// Add the create item and create quicklinks buttons.
echo $result->create_quicklinks_button();

$table->out(25, false);
// Footer.
echo $OUTPUT->footer();
