<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * language information
 *
 * @package    aceaddon_quicklinks
 * @copyright  2023 LMSACE Dev Team <lmsace.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = "LMSACE Quicklinks";
$string['configtitle'] = "Acetools / LMSACE Quicklinks";
$string['quicklinks'] = 'LMSACE Quick links ';
$string['title'] = "Title";
$string['linkimage'] = 'Quick link image';
$string['link'] = 'Quick link URL';
$string['icon'] = 'Icon';
$string['numberoflnks'] = 'Number of quick links';
$string['updatesuccess'] = 'Updated successfully';
$string['insertsuccess'] = "Inserted successfully";
$string['strquicklinks'] = 'Lmsace Quick links';
$string['createquicklinks'] = 'Create Quick Links';
$string['deleteconfirm'] = 'Are you sure you want to delete this quicklinks from the LMSACE quicklinks?';
$string['quicklinknotfound'] = 'Quick links not found';
$string['quicklinksdeleted'] = 'Quick link deleted successfully';
$string['quicklinks_help'] = 'Users Quick links';
$string['quicklinkicon'] = 'Quick link icon';