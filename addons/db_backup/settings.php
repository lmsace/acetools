<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin administration pages are defined here.
 *
 * @package     aceaddon_db_backup
 * @category    admin
 * @copyright   2023 LMSACE Dev Team <lmsace.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
global $CFG;

if ($hassiteconfig) {

    $page = new admin_settingpage('aceaddon_db_backup', get_string('pluginname', 'aceaddon_db_backup'));

    $name = 'aceaddon_db_backup/backupstatus';
    $title = get_string('backupstatus', 'aceaddon_db_backup');
    $description = get_string('backupstatusdesc', 'aceaddon_db_backup');
    $option = [
        1 => get_string('enable', 'aceaddon_db_backup'),
        0 => get_string('disable', 'aceaddon_db_backup'),
    ];
    $setting = new admin_setting_configselect($name, $title, $description, 1, $option);
    $page->add($setting);

    $name = 'aceaddon_db_backup/maxbackupdays';
    $title = get_string('maxbackupdays', 'aceaddon_db_backup');
    $description = get_string('maxbackupdaysdesc', 'aceaddon_db_backup');
    $setting = new admin_setting_configtext($name, $title, $description, '30', PARAM_INT);
    $page->add($setting);

    $name = 'aceaddon_db_backup/backupmoodledata';
    $title = get_string('backupmoodledata', 'aceaddon_db_backup');
    $description = get_string('backupmoodledatadesc', 'aceaddon_db_backup');
    $setting = new admin_setting_configcheckbox($name, $title, $description, 1);
    $page->add($setting);

    $name = 'aceaddon_db_backup/backuppath';
    $title = get_string('backuppath', 'aceaddon_db_backup');
    $description = get_string('backuppathdesc', 'aceaddon_db_backup');
    $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_TEXT, "size=70");
    $page->add($setting);

    $name = 'aceaddon_db_backup/compress';
    $title = get_string('compress', 'aceaddon_db_backup');
    $description = get_string('compressdesc', 'aceaddon_db_backup');
    $option = [
        \aceaddon_db_backup\dump_db::PHPGZIP => get_string('phpgzip', 'aceaddon_db_backup'),
        0 => get_string('disable', 'aceaddon_db_backup'),
    ];
    $dump = new \aceaddon_db_backup\dump_db();
    $info = '';
    if ($dump->find_gzip_isavailable()) {
        $option[\aceaddon_db_backup\dump_db::GZIPCMD] = get_string('gzipcmd', 'aceaddon_db_backup');
    } else {
        $info .= get_string('gzipcmdmissing', 'aceaddon_db_backup');
    }

    $setting = new admin_setting_configselect($name, $title, $description, 1, $option);
    $page->add($setting);

    // Verify the handler is avialble for current db, and the required libraries are available to preform the backup,
    // Otherwise display the warining to the admin.
    $libraries = (new \aceaddon_db_backup\dump_db())->verify_libraries();
    $info .= ($libraries !== true) ? $libraries : '';
    if ($info != '') {
        $setting = new admin_setting_description("librarymissing", "", get_string('librarymissing', 'aceaddon_db_backup', $info) );
        $page->add($setting);
    }

	// TODO: Add backup from and untill date picker.
    // TODO: Add ignore tables list.
    // TODO: Options to mysql command.
}
