<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Aceaddon db_backup - Define mysql db class file.
 *
 * @package    aceaddon_db_backup
 * @copyright  2023 LMSACE Dev Team <lmsace.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace aceaddon_db_backup\database;

/**
 * DB handler for mysql type databases.
 */
class mysql_db extends dbhandler {

    /**
     * Verify the needed libraries are available in server to create backup.
     *
     * @return void
     */
    public function verify_libraries() {

        $mydumpcmd = 'mysqldump --version'; // Command to check mysqldump availability.
        $output = '';
        $returnvar = 0;

        exec($mydumpcmd, $output, $returnvar);

        if ($returnvar !== 0) {
            set_config('backupstatus', 0, 'aceaddon_db_backup');
        }

        return ($returnvar === 0) ? true : get_string('mysqldumpnotfound', 'aceaddon_db_backup');
    }

    /**
     * Verify the current method is handled by this class.
     *
     * @param object $dbtype Db type.
     * @return bool True if the current db structure is handled by this class otherwise false.
     */
    public function handles($dbtype) : bool {
        return ($dbtype === 'mysqli' || $dbtype === "mariadb") ? true : false;
    }

    /**
     * Set databse config list.
     *
     * @param object $config Filename and path to store backups.
     * @return void
     */
    public function set_config($config) {
        $this->config = $config;
    }

    /**
     * Define file extension type for the backup.
     *
     * @return void
     */
    public function file_extension() {
        return '.sql';
    }


    /**
     * Build the commands to dumb the database to file.
     *
     * @return string
     */
    public function build_dump_command() : string {
        global $CFG;

        $dboptions = ' --skip-lock-tables --default-character-set=utf8mb4 --opt -C -e --create-options ';
        $dbauth = sprintf('--user=%s ', escapeshellarg($CFG->dbuser) );
        $dbauth .= sprintf('--password=%s ', escapeshellarg($CFG->dbpass));
        $dbauth .= sprintf('--host=%s ', escapeshellarg($CFG->dbhost));
        if (isset($CFG->dboptions['dbport']) && !empty($CFG->dboptions['dbport'])) {
            $dbauth .= sprintf('--port=%s ', escapeshellarg($CFG->dboptions['dbport']));
        }

        $filename = $this->config->storepath . '\\' . $this->config->filename;

        $db = sprintf(' %s > %s', $CFG->dbname, $filename);

        return sprintf('mysqldump %s %s %s', $dboptions, $dbauth, $db);
    }

}
