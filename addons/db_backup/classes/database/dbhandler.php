<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Aceaddon db_backup - Define DB handler class file.
 *
 * @package    aceaddon_db_backup
 * @copyright  2023 LMSACE Dev Team <lmsace.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace aceaddon_db_backup\database;

/**
 * Abstract class for db handlers - LMSACE DB Backup.
 */
abstract class dbhandler {

    /**
     * Config values for backup it contains Filename and path to store backups.
     *
     * @var stdClass
     */
    protected $config;

    /**
     * Verify the current method is handled by this class.
     *
     * @param object $dbtype Db type.
     * @return bool True if the current db structure is handled by this class otherwise false.
     */
    abstract public function handles($dbtype) : bool;

    /**
     * Set databse config list.
     *
     * @param object $config DB data.
     * @return void
     */
    abstract public function set_config($config);

    /**
     * Build the commands to dumb the database to file.
     *
     * @return string
     */
    abstract public function build_dump_command() : string;

    /**
     * Verify the needed libraries are available in server to create backup.
     *
     * @return void
     */
    abstract public function verify_libraries();

    /**
     * Execute custom actions after the dump completed.
     *
     * @return void
     */
    public function after_dump() {
        return false;
    }
}
