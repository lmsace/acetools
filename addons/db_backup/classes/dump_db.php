<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Aceaddon db_backup - Define dumb db class file.
 *
 * @package    aceaddon_db_backup
 * @copyright  2023 LMSACE Dev Team <lmsace.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace aceaddon_db_backup;

use aceaddon_db_backup\database\mysql_db;
use aceaddon_db_backup\database\pgsql_db;
use core\output\notification;
use moodle_exception;

require_once("$CFG->libdir/filestorage/zip_archive.php");

class dump_db {

    public const GZIPCMD = 2;

    public const PHPGZIP = 1;

    protected $db_handlers = [];

    protected $handler;

    public function __construct() {
        $this->db_handlers[] = new mysql_db;
        $this->db_handlers[] = new pgsql_db;
    }


    public function get_backup_day() {
        global $CFG;

        $completeddays = get_config('aceaddon_db_backup', 'completeddays');
        $currentday = $completeddays + 1;

        $maxdays = get_config('aceaddon_db_backup', 'maxbackupdays');
        // 0 means unlimited.
        if (!$maxdays) {
            return $currentday;
        }

        if ($currentday > $maxdays) {
            $currentday = 1;
        }

        return $currentday;
    }

    public function set_completed($day) {

        set_config('completeddays', $day, 'aceaddon_db_backup');
        // TODO: Set config change log.
    }

    public function execute() {
        global $CFG;

        // TODO: Verify any previous faiures during the dump then create the admin notification.

        // Find the configured db backup cycle, and its current count.
        $day = $this->get_backup_day();

        mtrace('DB-backup initiate for the cycle of day -' . $day );

        // # Create file location.
        if (get_config('aceaddon_db_backup', 'backuppath')) {
            $filedir = get_config('aceaddon_db_backup', 'backuppath'). "\aceaddon_db_backup";
        } else {
            $filedir = $CFG->dataroot."\aceaddon_db_backup";
        }
        // Verify the directory to store the backup is exists, if not creates a new one.
        if (!check_dir_exists($filedir) || make_writable_directory($filedir)) {
            // TODO: throw error.
            mtrace("...error occurred, $filedir path doesn't exist");
        }

        $this->handler = $this->get_handler();

        if (!$this->handler) {
            return false;
        }
        if (get_config('aceaddon_db_backup', 'backupmoodledata')) {
            $this->backup_moodle_data($filedir, $day);
        }

        $date = date('Y-m-d');
        // # BUild commands.
        $config = (object) [
            'day' => $day,
            'storepath' => $filedir,
            'filename' => $CFG->dbname."-backup-". $date .'-'.$day,
            'compress' => get_config('aceaddon_db_backup', 'compress'),
        ];

        $command = $this->build_commands($config);

        if ($command == false) {
            // TODO: Notification.
        }

        // Output setup.
        $output = '';
        $returnvar = 0;
        // # Take dump - Run command using shell.
        exec($command, $output, $returnvar);



        if ($returnvar === 0) {

            $dumpgzip = true;
            if ($config->compress == self::GZIPCMD && $this->find_gzip_isavailable()) {
                // Gzip command line utility is available

                $command = $this->compress_command($config);
                $compressReturn = 0;
                $compressOutput = '';
                exec($command, $compressOutput, $compressReturn);
                // Proceed with using the command line tool.
                if ($compressReturn === 0) {
                    $dumpgzip = false; // Compresssion success, Don't need to compress using php gzip library.
                    return true;
                }
            }

            // Run the compress using gzip library, if server doesn't contain gzip command or failed to compress.
            if ($dumpgzip && $config->compress != 0) {
                // Gzip command line utility is not available or Failed.
                // Fall back to using PHP's gzip library
                $this->compress_dump($config);
            }
        }

        if ($returnvar === 0) {
            mtrace('Moodle database backup completed');
        } else {
            mtrace($output);
        }

        // # Compress if available.
        $this->set_completed($day);

        if ($this->handler) {
            $this->handler->after_dump();
        }
    }

    public function backup_moodle_data($filedir, $day) {
        global $CFG;

        mtrace("Ready to backup the moodle data");

        $rootPath = realpath($CFG->dataroot);
        $date = date('Y-m-d');
        // Initialize archive object
        $zip = new \ZipArchive();
        $file = $filedir. "/dataroot_backup-$date-$day" .".zip";

        if ($zip->open($file, \ZipArchive::CREATE | \ZipArchive::OVERWRITE)) {

            // Create recursive directory iterator
            $files = new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator($rootPath),
                \RecursiveIteratorIterator::LEAVES_ONLY
            );

            $directoryContents = scandir($rootPath);
            foreach ($files as $name => $file)
            {
                // Skip directories (they would be added automatically)
                if (!$file->isDir())
                {
                    // Get real and relative path for current file
                    $filePath = $file->getRealPath();
                    $relativePath = substr($filePath, strlen($rootPath) + 1);

                    // Add current file to archive
                    $zip->addFile($filePath, $relativePath);
                }
            }

        }
    }

    protected function get_handler() {
        global $CFG;

        $dbtype = $CFG->dbtype;
        foreach ($this->db_handlers as $handler) {
            if ($handler->handles($dbtype)) {
                return $handler;
            }
        }

        return false;
    }


    protected function build_commands(&$config) {

        // Verify the DB type and get the dump handler.
       // $handler = $this->get_handler();

        if (!$this->handler) {
            return false;
        }

        // Use the config to send other data to db handlers, used this method for future purpose to extend the configurations.
        $this->handler->set_config($config);

        $config->filename .= $this->handler->file_extension();

        // Create build commands.
        $commands = $this->handler->build_dump_command();

        return $commands;
    }

    public function find_gzip_isavailable() {

        $gzipcmd = 'gzip --version'; // Command to check gzip availability.
        $output = '';
        $returnvar = 0;

        exec($gzipcmd, $output, $returnvar);

        return ($returnvar === 0) ? true : false;
    }

    protected function compress_command($config) {

        $sqlfile = $config->storepath . '/'.$config->filename;

        $gzipCommand = sprintf('gzip %s', $sqlfile.'.gz');

        return $gzipCommand;
    }

    protected function compress_dump($config) {
        global $CFG;

        $sqlfile = $config->storepath . '/'.$config->filename;
        $compressedFilePath = $sqlfile . '.gz';

        // Verfiy the compressed file for the day cycle count.
        if (file_exists($compressedFilePath)) {
            unlink($compressedFilePath);
            mtrace('Removed old backup for this count - '.$config->day);
        }


        $sourceFileHandle = fopen($sqlfile, 'rb');
        // Open the gz file (w9 is the highest compression)
        $compressedFileHandle = gzopen($compressedFilePath, 'w9');

        while (!feof($sourceFileHandle)) {
            $buffer = fread($sourceFileHandle, 4096);
            gzwrite($compressedFileHandle, $buffer);
        }

        fclose($sourceFileHandle);
        gzclose($compressedFileHandle);

        // Remove the sql file. once the gz compress completed.
        unlink($sqlfile);
    }

    public function verify_libraries() {

        $handler = $this->get_handler();

        if (!$handler) {
            return get_string('dbhandlersnotavailable', 'aceaddon_backup');
        }

        return $handler->verify_libraries();
    }
}
