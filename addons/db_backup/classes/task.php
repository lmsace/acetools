<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Task execution to create db backup.
 *
 * @package   aceaddon_db_backup
 * @copyright 2023 LMSACE Dev Team <lmsace.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace aceaddon_db_backup;

/**
 * Task handler, creates the database backup when the schedule task triggered.
 */
class task extends \core\task\scheduled_task {

    /**
     * File name of the backup sql.
     *
     * @var string
     */
    public $filename = '';

    /**
     * Name of the task.
     */
    public function get_name() {
        return get_string('pluginname', 'aceaddon_db_backup');
    }

    /**
     * Triggered when the cron running and the task run time meets the currenttime.
     * Creates the custom upload direcotry and make the db backup using mysqldump.
     *
     * @return void
     */
    public function execute(): void {
        global $CFG;

       $status = get_config('aceaddon_db_backup', 'backupstatus');
        if ($status == 1) {
            // Call the dumb_db.
            $dumb = new \aceaddon_db_backup\dump_db();

            $dumb->execute();
        }
    }

}
