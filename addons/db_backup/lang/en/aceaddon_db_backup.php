<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * String language file.
 *
 * @package   aceaddon_db_backup
 * @copyright 2023 LMSACE Dev Team <lmsace.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'DB Backup';
$string['maxbackupdays'] = 'Max days to run backup';
$string['maxbackupdaysdesc'] = 'Enter the days in number to get the backups. 0 for unlimited days';
$string['backupstatus'] = 'DB Backup status';
$string['backupstatusdesc'] = 'Enable / disable the db backup option';
$string['enable'] = 'Enable';
$string['disable'] = 'Disable';
$string['privacy:metadata'] = 'The DB Backup plugin does not store any personal data any specific user data,
It creates a backup for entire DB and store it in moodle data folder.';
$string['mysqldumpnotfound'] = '<span class="error"> Backup disabled, <b> mysqldump </b> command line utility not available.</span><br>';
$string['pgsqldumpnotfound'] = '<b> pg_dump </b> command line utility not available.<br>';

$string['compress'] = 'Compress the backup file';
$string['compressdesc'] = 'Compress the db file after the successfully dump. It compress the file using gzip, it helps to reduce the file size';
$string['phpgzip'] = 'PHP-Gzip';
$string['gzipcmd'] = 'Gzip-cmd';
$string['gzipcmdmissing'] = '<b> Gzip </b> command line utility is not available on the server. It is recommended for larger databases.<br>  ';
$string['librarymissing'] = '<h3> Missing Libraries </h3> <br> {$a}';
$string['backuppath'] = "Backup file path location";
$string['backuppathdesc'] = "Backup file path location";
$string['backupmoodledata'] = "Backup moodle data directory";
$string['backupmoodledatadesc'] = "";
