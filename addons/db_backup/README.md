Aceaddon DB Backup Plugin
==============================

The DB Backup plugin is a convenient and reliable tool that allows you to easily backup your Moodle database. It helps maintain backups for a specified number of days in a cycle and overwrites the backup once the maximum configured days are reached. Additionally, it compresses the backup database file using gzip, reducing the file size by more than 30%.


### Version

Plugin version: 1.0

Released on: 16 July 2023

Authors: https://lmsace.com, LMSACE Dev Team

### Plugin Repository

Public Git Repository
[https://github.com/lmsace/lmsace-db-backup](https://github.com/lmsace/lmsace-db-backup)


## Dependencies

Command Line Utilities: 

* "**mysqldump**" for MySql and MariaDB.
* "**pg_dump**" for PostgressSQL
* "**gzip**" Command line utility or "**PHP Gzip**" for compress

## Documenation

The official documentation for LMSACE Connect can be found at [https://lmsace.com/docs/lmsace-db-backup](https://lmsace.com/docs/lmsace-db-backup).

## Bugs and Problems Report.

This plugin is carefully developed and highly tested with multiple version of Moodle for MySqli, MariaDB and PostgresSql. If you found any issues with LMSACE DB Backup or you have any problems or difficulty with its workflow please feel free to report on Github: http://github.com/lmsace/lmsace-db-backup/issues. We will do our best to solve the problems.

## Feature Proposals.

You are develop custom feature for DB Backup. Please create pull request and create issue on Github: https://github.com/lmsace/lmsace-db-backup/issues.


## Features.

1. Create database backup dumps (Mysqli, MariaDB and PostgresSql) .
2. Compress the database backup files.
3. Restore the backup to database (Upcoming Version)

### Installation steps using ZIP file.


1. Download the '**lmsace-db-backup.zip**' from the githu [releases](https://github.com/lmsace/lmsace-db-backup/releases) .

2. Next login as Site administrator

3. Go to '*Plugins*' -> '*Add New*' -> '*Upload Plugin*', On here upload the plugin zip '**lmsace-db-backup.zip**'.

4. Click the "**Instal Now**" button.

> You will get **success message** once the plugin installed successfully.

6. By clicking "**Activate**" button on success page , Plugin will activated and you will redirect to Your dashboard.


### Installation steps using Git.


1. Clone **LMSACE Connect** plugin Git repository into the folder '*plugins*'.

> '*Your root diroctory*' -> '*wp-content*' -> '*plugins*'.

2. Rename the folder name into '**lmsace-connect**'.

3. Next login as Site administrator

4. Go to the '*Plugins*' -> '*Installed plugins*'. On here LMSACE Connect plugin will listed.

5. Activate the plugin by clicking "**Activate**" button placed on the bottom of the plugin name, Plugin will get the success message.


## Steps to setup connection.

> Please install the moodle side plugin and generate webservice token in your Moodle LMS.<br> Follow the steps mentioned on the readme to generate token. [https://github.com/lmsace/lmsace-connect-moodle](https://github.com/lmsace/lmsace-connect-moodle)

1. Log in as site administrator and go to admin backend.

2. Go to the **LMSACE Connect**.

3. Enter your Moodle Site URL and Site token which you can copy from your Moodle **LMSACE Connect** "*General*" tab.

4. Please click the '**Save Change**' button to save the details.

![Connection Setup](https://www.lmsace.com/docs/lmsace-connect/images/connection-setup.png)

> After you have saved the details, you can get the notification about the connection status. If you received any errors then please check the given details.

5. Once you completed the connection setup successfully. Please goto the "**General Setup**" tab.

6. In General setup, Please select the role for a customer user. The selected role will be assigned for the pariticipants who enrolled in the course from your WooCommerce.

![Connection Setup](https://www.lmsace.com/docs/lmsace-connect/images/general-setup.png)

## Import courses as products.


> **LMSACE Connect** brings the course import as much easier compare with other moodle + woocommerce integrations options.

1. Goto the "**Import courses**" tab in "**LMSACE Connect**" admin menu.

2. In Import page, you can view the list of courses available in the connected Moodle LMS instance.

3. Click the checkbox to select the courses you want to import.

4. Select any or all options based on your need. After the course table, You can found the various "***Import Options***".

5. Click the "**Start Import Courses**" button to import the selected courses.

> **LMSACE Connect** will import the courses in background using WP_Schedule if you have tried to import more than 25 courses in single time.

![Import Courses](https://www.lmsace.com/docs/lmsace-connect/images/import-course.png)


## Copyright

LMSACE DEV TEAM https://lmsace.com

### Review

You feel LMSACE Connect helps you to sell courses easier. Give a review and rating on wordpress.org. We are looking for your comments.











Version 1.0 Free
------------------

Create a dbbackup for configured no of days in a cycle
Compress the Backups

Version 2.0 Pro
------------------

Manage Backups (Table with delete options)
Restore any backup db, (Create backup before dump)
Initiate the backup by own
Scheduled time backup
Maintain multiple backups for a day for each day in cycle.
Ignore tables on backup (Helps to Remove standard log tables)

