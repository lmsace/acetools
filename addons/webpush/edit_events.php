<?php

// Require config.

use local_acetools\plugininfo\aceaddon;

require(__DIR__.'/../../../../config.php');
require_once($CFG->libdir . '/adminlib.php');
// TODO: Capability check.
// require_capability();
// require_sesskey();

// Event ID to edit.
$id = optional_param('id', null, PARAM_INT);
$action = optional_param('action', null, PARAM_ALPHAEXT);

admin_externalpage_setup('reporteventlists');

// Page values.
$url = new moodle_url('/local/acetools/addons/webpush/edit_events.php');
$context = context_system::instance();

// Setup page values.
$PAGE->set_url($url);
$PAGE->set_context($context);
$PAGE->set_heading(get_string('pluginname', 'aceaddon_webpush'));

$PAGE->navbar->add(get_string('themes', 'core'), new moodle_url('/admin/category.php', array('category' => 'themes')));
$PAGE->navbar->add(get_string('pluginname', 'aceaddon_webpush'), new moodle_url('/admin/category.php',
        array('category' => 'aceaddon_webpush')));
$PAGE->navbar->add(get_string('pluginname', 'aceaddon_webpush'), new moodle_url('/local/acetools/addons/webpush/edit.php'));

// Edit event form.
$eventform = new aceaddon_webpush\event_form(null, ['id' => $id]);
$overviewurl = new moodle_url('/local/acetools/addons/webpush/events.php');

// UInsert or update the event data.
if ($formdata = $eventform->get_data()) {
    $result = aceaddon_webpush\event::manage_instance($formdata);
    if ($result) {
        redirect($overviewurl);
    }
} else if ($eventform->is_cancelled()) {
    redirect($overviewurl);
}


// Setup the event to the form, if the form id param available.
if ($id !== null && $id > 0) {

    if ($record = aceaddon_webpush\event::get_event($id)) {
        // print_object($record);exit;
        // Set the event data to the event edit form.
        $eventform->set_data($record);
    } else {
        // Direct the user to list page with error message, when the requested event is not available.
        \core\notification::error(get_string('recordmissing', 'aceaddon_webpush'));
        redirect($overviewurl);
    }
}

// Page content display started.
echo $OUTPUT->header();

echo $OUTPUT->heading(get_string('eventsnotification', 'aceaddon_webpush'));

// Display the smart event form for create or edit.
echo $eventform->display();

// Retrieve all events in a list.
$completelist = report_eventlist_list_generator::get_all_events_list();

$tabledata = array();
$components = array();
$edulevel = array('0' => get_string('all', 'report_eventlist'));
$crud = array('0' => get_string('all', 'report_eventlist'));
foreach ($completelist as $value) {
    $components[] = explode('\\', $value['eventname'])[1];
    $edulevel[] = $value['edulevel'];
    $crud[] = $value['crud'];
    $tabledata[] = (object)$value;
}
$components = array_unique($components);
$edulevel = array_unique($edulevel);
$crud = array_unique($crud);

// Create the filter form for the table.
$filtersection = new report_eventlist_filter_form(null, array('components' => $components, 'edulevel' => $edulevel,
        'crud' => $crud));

// Output.
// $renderer = $PAGE->get_renderer('report_eventlist');
$server = new \aceaddon_webpush\webpushserver();

$list = $server->render_event_list($filtersection, $tabledata);
echo html_writer::div($list, 'events-list-filter');

// Hide the events list filter by fieldset click.
$PAGE->requires->js_amd_inline('require(["jquery"], function($) {
    window.onload = function() {
        var listTable = $(".report-eventlist-data-table");
        var filter = $(".events-list-filter form fieldset a");
        console.log(filter);
        filter[0].click();
        listTable.hide();
        $(filter[0]).click(function() {
            var fieldset = $(this).parents("fieldset");
            if (fieldset.hasClass("collapsed")) {
                listTable.show();
            } else {
                listTable.hide();
            }
        })
    };
})');

// Footer.
echo $OUTPUT->footer();
