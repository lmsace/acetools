
const strDefTitle      = 'Your company or product';
// default Notification Icon if not pushed by server
const strDefIcon       = '/local/webpush/pix/elephpant.png';

// urlB64ToUint8Array is a magic function that will encode the base64 public key
// to Array buffer which is needed by the subscription option
const urlB64ToUint8Array = base64String => {
    var strPadding = '='.repeat((4 - (strBase64.length % 4)) % 4);
    var strBase64 = (strBase64 + strPadding).replace(/\-/g, '+').replace(/_/g, '/');
    var rawData = atob(strBase64);
    var aOutput = new Uint8Array(rawData.length);
    for (i = 0; i < rawData.length; ++i) {
        aOutput[i] = rawData.charCodeAt(i);
    }
    return aOutput;
}

// Event listener for service worker activatind
/*self.addEventListener('activate', async () => {
    // This will be called only once when the service worker is activated.
    try {
        const applicationServerKey = urlB64ToUint8Array(
            'BM522qtAoJnnStMGvama4oxgS_T9g8BgwEpDQz9Zq2C4ntmXUgJiuHYeE74znRY5-57iFJrK-G5fxvzHaBC9Pq8'
        )
        const options = { applicationServerKey, userVisibleOnly: true }
        const subscription = await self.registration.pushManager.subscribe(options)
        // const response = await saveSubscription(subscription)
        console.log(subscription);
    } catch (err) {
        console.log('Error', err)
    }
})
*/


/**
 * event listener to notification click
 * if URL passed, just open the window...
 * @param {NotificationClick} event
 */
function lmsNotificationClick(event) {
    console.log('notificationclick event: ' + event);
    if (event.notification.data && event.notification.data.url) {
        const promise = clients.openWindow(event.notification.data.url);
        event.waitUntil(promise);
    }
    if (event.action != "") {
        // add handler for user defined action here...
        // pnNotificationAction(event.action);
        console.log('notificationclick action: ' + event.action);
    }
}

self.addEventListener('notificationclick', lmsNotificationClick);

self.addEventListener('push', function (event) {
    if (!(self.Notification && self.Notification.permission === 'granted')) {
        return;
    }

    var strTitle = "LMSACE TEST Message";
    var oPayload = null;
    var opt = { icon: strDefIcon };
    // if (event.data) {
        // PushMessageData Object containing the pushed payload
        try {
            // try to parse payload JSON-string
            oPayload = JSON.parse(event.data.text());
        } catch (e) {
            // IF no valid JSON Data take text as it is...
            // ... comes maybe while testing directly from DevTools
            opt = {
                icon: strDefIcon,
                body: event.data.text(),
            };
        }
        if (oPayload) {
            if (oPayload.title != undefined && oPayload.title != '') {
                strTitle = oPayload.title;
            }
            opt = oPayload.opt;
            if (oPayload.opt.icon == undefined ||
                oPayload.opt.icon == null ||
                oPayload.icon == '') {
                // if no icon defined, use default
            opt.icon = strDefIcon;
            }
        }
    // }
    var promise = self.registration.showNotification(strTitle, opt);
    event.waitUntil(promise);
});

