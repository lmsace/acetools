
define(['jquery', 'core/templates', 'core/fragment', 'core/str', 'core/modal_factory', 'core/modal_events', 'core/storagewrapper'],
	function($, Templates, Fragment, Str, Modal, ModalEvents, Storagewrapper) {


	const permissionID = "enable-web-notification";

	/**
	 * Verify the user current browser supports the service worker and push manager.
	 */
	const checkPushApi = () => {
		if (!('serviceWorker' in navigator)) {
			throw new Error('No Service Worker support!');
		}
		if (!('PushManager' in window)) {
			throw new Error('No Push API Support!');
		}
	};

	// Display the notification to allow the LMS to send push notifications.
	const display_information = () => {

		if (sessionStorage.getItem('notificationcanceled')) {
			return;
		}

		Modal.create({
            title: Str.get_string('enablenotification', 'aceaddon_webpush'),
            body: Templates.render('aceaddon_webpush/enablenotification', {}),
            large: true,
            draggable: true,
			type: Modal.types.SAVE_CANCEL
        }).done((modal) => {
			modal.setButtonText('save', Str.get_string('enable'));

            modal.getRoot().on(ModalEvents.save, function(e) {
                requestNotificationPermission();
				modal.hide();
            });

			modal.getRoot().on(ModalEvents.cancel, function(e) {
                // showAlternateMethod();
				sessionStorage.setItem('notificationcanceled', true);
            });

			modal.show();
        })
	};

	const urlB64ToUint8Array = strBase64 => {
		var strPadding = '='.repeat((4 - (strBase64.length % 4)) % 4);
		var strBase64 = (strBase64 + strPadding).replace(/\-/g, '+').replace(/_/g, '/');
		var rawData = atob(strBase64);
		var aOutput = new Uint8Array(rawData.length);
		for (var i = 0; i < rawData.length; ++i) {
			aOutput[i] = rawData.charCodeAt(i);
		}
		return aOutput;
	};


	const registerServiceWorker = async() => {
		const swRegistration = await navigator.serviceWorker.register(M.cfg.wwwroot + '/local/acetools/addons/webpush/js/service.js').then((registration) => {
			return registration;
		});
		return swRegistration;
	};

	/**
	 * Subscribe the user to receive the notifications.
	 *
	 * @returns
	 */
	const subscribeUserPush = async() => {
		return registerServiceWorker().then(function(registration) {
			const subscribeOptions = {
				userVisibleOnly: true,
				applicationServerKey: urlB64ToUint8Array(
					webpush.publickey
				)
			};
			return registration.pushManager.subscribe(subscribeOptions);
		})
		.then(async(subscription) => {
			// Console.log('Received PushSubscription: ', JSON.stringify(pushSubscription));
			subscription = JSON.parse(JSON.stringify(subscription));
			subscription.userAgent = navigator.userAgent;
			var data = JSON.stringify(subscription);
			console.log(webpush.context);
			Fragment.loadFragment('aceaddon_webpush', 'store_subscription', webpush.context, {subscription: data}).then(function(response) {
				return true;
			})
		});
	};

	/**
	 * REquest the notificationpermission to user.
	 */
	const requestNotificationPermission = async() => {
		const permission = await window.Notification.requestPermission();
		// Value of permission can be 'granted', 'default', 'denied'
		// granted: user has accepted the request
		// default: user has dismissed the notification permission popup by clicking on x
		// denied: user has denied the request.
		if (permission == 'granted') {
			try {
				// Subscribe the user to received the notifications.
				subscribeUserPush();
			} catch (err) {
				throw new Error('Permission not granted for Notification');
			}
		} else if (permission == 'denied') {
			// TODO: if needed send the error to LMS admin.
			console.log('Browser push notification denied by user');
		}
	};

	// Init the push notification
	// Check the push api is available
	// Check the notification permission is granted.
	const main = async() => {
		checkPushApi();
		const swRegistration = await registerServiceWorker();
		// Display the notification enable message.
		if (window.Notification.permission != 'granted' && window.Notification.permission != 'denied') {
			display_information();
		}
	};

	return {
		init: (webpush) => {
			if (webpush.initservice) {
				main();
			}
		}
	};

});
