-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2023 at 04:48 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `moodle_310`
--

-- --------------------------------------------------------

--
-- Table structure for table `mdl_aceaddon_webpush_events`
--

CREATE TABLE `mdl_aceaddon_webpush_events` (
  `id` bigint(20) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `event` varchar(500) NOT NULL,
  `subject` text DEFAULT NULL,
  `message` longtext DEFAULT NULL,
  `message_format` tinyint(4) DEFAULT 1,
  `relateduser` tinyint(4) DEFAULT 1,
  `eventuser` tinyint(4) DEFAULT 0,
  `roles` text DEFAULT NULL,
  `role_context` int(2) DEFAULT 1,
  `cohorts` text DEFAULT NULL,
  `method` int(2) DEFAULT 1,
  `visible` int(1) NOT NULL DEFAULT 1,
  `timemodified` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mdl_aceaddon_webpush_events`
--

INSERT INTO `mdl_aceaddon_webpush_events` (`id`, `title`, `description`, `event`, `subject`, `message`, `message_format`, `relateduser`, `eventuser`, `roles`, `role_context`, `cohorts`, `method`, `visible`, `timemodified`) VALUES
(14, '', '', '\\mod_quiz\\event\\attempt_submitted', '', '', 1, 1, 0, '[\"1\",\"3\",\"4\"]', 1, '[]', 1, 1, NULL),
(15, 'Coursemodulecreated', 'Send notification to course users about new module created.', '\\core\\event\\course_module_created', 'New module created', '<p dir=\"ltr\" style=\"text-align: left;\">Hi {User_Firstname},</p><p dir=\"ltr\" style=\"text-align: left;\"><br>New Module {event_title} created in {event_course},<br><br>Thanks,</p>', 1, 1, 1, '[\"3\",\"5\"]', 1, '[]', 1, 1, NULL),
(16, 'Coursemodulecreated', 'Send notification to course users about new module created.', '\\core\\event\\course_module_created', 'New module created', '<p dir=\"ltr\" style=\"text-align: left;\">Hi {User_Firstname},</p><p dir=\"ltr\" style=\"text-align: left;\"><br>New Module {event_title} created in {event_course},<br><br>Thanks,</p>', 1, 1, 1, '[\"3\",\"5\"]', 1, '[]', 1, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mdl_aceaddon_webpush_subs`
--

CREATE TABLE `mdl_aceaddon_webpush_subs` (
  `id` bigint(20) NOT NULL,
  `userid` bigint(20) NOT NULL,
  `subscription_json` text NOT NULL,
  `timecreated` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mdl_aceaddon_webpush_subs`
--

INSERT INTO `mdl_aceaddon_webpush_subs` (`id`, `userid`, `subscription_json`, `timecreated`) VALUES
(1, 3, '{\"endpoint\":\"https://fcm.googleapis.com/fcm/send/cC0xnNsYIe8:APA91bF0lG-dziDjiWlnJ8F868WTSLucpxw3DrnzKZv3iN3PEX4xxxptN8kw5Tue21c0vIyY3Wpq7Vlvopk_6_dLVhefF41EpJiZiKozYKY-k_Sl95E1NAJyobur-mP7O3fOZfZD7ub7\",\"expirationTime\":null,\"keys\":{\"p256dh\":\"BOR1ES0z94c4a3ynT8rCERySmz-P_nHs1VFr34r6hlZutZIjccHwoMSKvVKf7JtCRdfMW1macBft08ljtIUqZP0\",\"auth\":\"pS07hwQQ8TbZPcmEcU4BJQ\"},\"userAgent\":\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36 OPR/98.0.0.0\"}', 1683256729),
(2, 2, '{\"endpoint\":\"https://fcm.googleapis.com/fcm/send/ewlk07ksBus:APA91bH6fef4Lh3CiJ-b8om8eUC5AEebTC6im2VIuuXa1wvt32Ac5Hy6brQLHXZb34gkt7mowUNzM_80-c5xB2O54CNjziH6HR4Uh41d7mLuNYYxP4ysMdn6oOsBDu9cP1lOlF1HvOOx\",\"expirationTime\":null,\"keys\":{\"p256dh\":\"BDkYt7E3MbWG0CTHYOW4i74mfkLpF1jC404IWOCUZh3bNutOiinqAJGtflSDxwwrFVOF95-rjfbzROYMqT31gQc\",\"auth\":\"VP2Q3reIocwYFDeij-XMpQ\"},\"userAgent\":\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36\"}', 1683164782),
(3, 0, '{\"endpoint\":\"https://fcm.googleapis.com/fcm/send/flI60e1NPM0:APA91bFRwSt-1J63h3ZLBxTO8mghN59owrfJ6k-Vtcxz4fzkjygoKcFLvpMDEx2GTFRXqQG9Y-GcXgQjaISCPAGz1UoFCrfrFXE4yhivgcABPiJROdGuzKiZ9WXZF4vhR9h7hKiLPu4U\",\"expirationTime\":null,\"keys\":{\"p256dh\":\"BEXpRajK7yW2xiISuP2XQDy8ppFL6lxN4J1XBmcxuXvfqxEPlnST4MRyez0DtNctvBYbHIqda6KEo8dnLkCxCHI\",\"auth\":\"AhtXQv3DG8cr3kUlGrdssw\"},\"userAgent\":\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36\"}', 1628986523),
(4, 4, '{\"endpoint\":\"https://wns2-pn1p.notify.windows.com/w/?token=BQYAAADT6KehaAShiZtrl%2bKZ792R2gFEeu%2bVA3l7%2bk6dHq%2fRRM5OnjQOiqpI8cEkfDeaEc5Kr8RF1hffOhsfmx3OC%2bMS5Q5G2lAlM4ykdFwA8IC4Bp4RYwDmY3yqQmhocvNbqWHdZ5uViltaFOaTwFdIAYUxnCfJKAkX4S%2f7Etl8Tvw35dAlk1fTJSIDlBpeNiD4gnim1i4N%2fIleBLgEpy5Rq1AxqHXypetjZYjI%2fshpSXbdEismU9dtfRKbi1sTv7VKm%2f7Ak6hgfdQ8CHoOMCahyM4jpASc8UquCkD%2b9PbdW0K3GarhC9R%2bfheqexvmCowceN0%3d\",\"expirationTime\":null,\"keys\":{\"p256dh\":\"BHQP046giPrf01yS-iZV9UYlEtUCM16k9RH-jRzlr3YswVqrbnf_siEmHK-hkHtC3M8teUVGQgWPo0worMvrjJs\",\"auth\":\"iUPgp_MtUnaiC7mN1MQx3g\"},\"userAgent\":\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36 Edg/112.0.1722.58\"}', 1683256845),
(5, 5, '{\"endpoint\":\"https://fcm.googleapis.com/fcm/send/fRDuG-U8e8c:APA91bENAlt0tbFYcs7sRGGawvEyH_XwYi4bh8uprnuLKQc4izml_v3y79HsIFhgD467OCRNgRhv1HXRRtUZSlUdnDMDDJu1-2JReVKsmpou8yw1oHIoZDGFGJ4Iy_yz0DflbFeZDnhi\",\"expirationTime\":null,\"keys\":{\"p256dh\":\"BDWN25gsMkpXNYOeEyXBdwog9kOgAbPqLIqRM52ByQBIcD3DIG81HrZRfusfqT3DjwGg6icJrFd_uHZj6gIsIqI\",\"auth\":\"43cWle_HYXq7foLZszZuvA\"},\"userAgent\":\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36 OPR/78.0.4093.147\"}', 1629029933);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mdl_aceaddon_webpush_events`
--
ALTER TABLE `mdl_aceaddon_webpush_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mdl_aceaddon_webpush_subs`
--
ALTER TABLE `mdl_aceaddon_webpush_subs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mdl_aceaddon_webpush_events`
--
ALTER TABLE `mdl_aceaddon_webpush_events`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `mdl_aceaddon_webpush_subs`
--
ALTER TABLE `mdl_aceaddon_webpush_subs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
