<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Aceaddon Webpush - Upgrade script.
 *
 * @package    aceaddon_webpush
 * @copyright  LMSACE Dev TEAM - 2023 
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Function to upgrade webpush ace addon.
 * 
 * @param int $oldversion the version we are upgrading from
 * @return bool result
 */
function xmldb_aceaddon_webpush_upgrade($oldversion) {
    global $DB, $OUTPUT;

    $dbman = $DB->get_manager();
   

    // Create events and subscriptions.
    if ($oldversion < 2023010512) {

        // Create the table for subscriptions.
        $table = new xmldb_table('aceaddon_webpush_subs');
        // Field definitions for the table aceaddon_webpush_subs.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '18', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table->add_field('userid', XMLDB_TYPE_INTEGER, '18', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null, 'id');
        $table->add_field('subscription_json', XMLDB_TYPE_TEXT, null, null, XMLDB_NOTNULL, null, null, 'userid');
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '18', null, null, null, null, 'subscription_json');        

        $table->add_key('primary', XMLDB_KEY_PRIMARY, ['id']);

        // Create the table if not already created.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Create the table for custom events to notify.
        $table = new xmldb_table('aceaddon_webpush_events');
        // Field definitions for the table aceaddon_webpush_events.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '18', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table->add_field('title', XMLDB_TYPE_CHAR, '255', null, null);
        $table->add_field('description', XMLDB_TYPE_TEXT, null, null, null, null, null, 'title');
        $table->add_field('description_format', XMLDB_TYPE_CHAR, '1', null, null, null, null, 'description');
        $table->add_field('event', XMLDB_TYPE_CHAR, '500', null, XMLDB_NOTNULL, null, null, 'description_format');
        $table->add_field('method', XMLDB_TYPE_INTEGER, '2', XMLDB_UNSIGNED, null, null, '1', 'event');
        $table->add_field('subject', XMLDB_TYPE_TEXT, null, null, null, null, null, 'method');
        $table->add_field('message', XMLDB_TYPE_TEXT, null, null, null, null, null, 'subject');
        $table->add_field('message_format', XMLDB_TYPE_CHAR, '1', null, null, null, null, 'message');
        $table->add_field('relateduser', XMLDB_TYPE_INTEGER, '2', XMLDB_UNSIGNED, null, null, null, 'message_format');
        $table->add_field('eventuser', XMLDB_TYPE_INTEGER, '2', XMLDB_UNSIGNED, null, null, '1', 'relateduser');
        $table->add_field('roles', XMLDB_TYPE_TEXT, null, null, null, null, null, 'eventuser');
        $table->add_field('rolecontext', XMLDB_TYPE_INTEGER, '2', null, XMLDB_NOTNULL, null, '1', 'roles');
        $table->add_field('cohorts', XMLDB_TYPE_TEXT, null, null, null, null, null, 'rolecontext');
        $table->add_field('start_date', XMLDB_TYPE_INTEGER, '18', null, null, null, null, 'languages');
        $table->add_field('end_date', XMLDB_TYPE_INTEGER, '18', null, null, null, null, 'start_date');
        $table->add_field('visible', XMLDB_TYPE_INTEGER, '1', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, '1', 'end_date');
        $table->add_field('timemodified', XMLDB_TYPE_INTEGER, '18', null, null, null, null, 'visible');
       
        $table->add_key('primary', XMLDB_KEY_PRIMARY, ['id']);

        // Create the table if not already created.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }
        // Update the boost_union theme savepoint.
        upgrade_plugin_savepoint(true, 2023010512, 'local', 'aceaddon_webpush');
    }

    return true;
}
