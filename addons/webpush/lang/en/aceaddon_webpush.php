<?php


$string['pluginname'] = 'WebPush';
$string['eventslist'] = 'Manage events notifications';

$string['publickey'] = 'Vapid Public key';
$string['publickey_description'] = 'Application Public key, You can generate your key pairs on here <a href="https://web-push-codelab.glitch.me/"> Create Vapid Keys </a>';
$string['privatekey'] = 'Vapid Private key';
$string['privatekey_description'] = 'Application Private key';
$string['mailto'] = 'Mail To';
$string['mailto_description'] = 'Mail To';
$string['managekeys'] = 'Manage keys';
$string['webpushdescription'] = 'Web push notifications settings';
$string['missinglibraries'] = 'The following libraries are used to send the push notifications,
Please install and enable the libraries <b> {$a} </b>';

$string['enablenotificationdesc'] = 'Enable the browser notification to receive notifications in your desktop';
$string['enablenotification'] = 'Enable Notification';

$string['sendtocohorts'] = 'Send to Cohorts';
$string['recipients'] = 'Notificaton recipients';
$string['title'] = 'Title';
$string['description'] = 'Description';
$string['event'] = 'Event';
$string['roles'] = 'Roles';
$string['cohorts'] = 'Cohorts';
$string['events_desc'] = 'In the "Manage Notifications" section, you can create and manage custom notifications tailored to specific notifications in your Moodle platform. This feature allows you to send personalized messages to your users based on their roles and cohorts.
<br>
To create a custom notification, click on the "Create new notification" button. Provide a descriptive title for the notification, such as "Assignment Due Date Reminder" or "Course Announcement." Next, specify the notification details, including the content of the notification. You can include relevant information, such as due dates, instructions, or any other important updates.
<br>
Select the target audience for the notification based on roles and cohorts. For example, you can choose to send the notification only to students, teachers, or a specific cohort of users. This ensures that the notification reaches the intended recipients and avoids unnecessary notifications for unrelated users.
<br>
Once you have configured the notification details and the target audience, save the notification. It will be added to the list of managed notifications.
From this list, you can easily edit or delete notifications as needed.
<br>
By creating custom notifications for specific events, you can effectively communicate important information and updates to your Moodle users. This feature enhances engagement and ensures that users receive relevant notifications, keeping them informed and engaged within the Moodle learning environment.';

$string['events'] = 'Events';
$string['eventdeleted'] = 'Event deleted successfully';
$string['eventduplicated'] = 'Event duplicated successfully';
$string['addnewevent'] = 'Create new notification';

$string['eventsnotification'] = 'Events Notifications';
$string['sendtorelateduser'] = 'Send to related user';
$string['sendtoeventuser'] = 'Send to event user';
$string['restrictions'] = 'Restrictions';

// Role options.
$string['roles_help'] = 'Roles to receive notificatoin';
$string['role_eventcontext'] = 'Event instance context roles';
$string['role_system_eventcontext'] = 'System + Event instance context roles';
$string['role_all'] = 'All role users';
$string['role_context'] = 'Role Context';
$string['role_context_help'] = 'Select roles to receive notification,
1. Event instance: Only users assigned in the events instance roles, ex any evnet in course, then it send to course roles users in that course instance';

// Date strings.
$string['notifiydateselector'] = 'Send notification within dates';
$string['from'] = 'from';
$string['until'] = 'until';
$string['from_help'] = 'Send the notification once the start time is reached';
$string['until_help'] = 'Stop sending notification to users once the time is reached';
$string['general'] = 'General';
$string['recepients'] = 'Recepients';
$string['notificationcontent'] = 'Notification content';
$string['sendmethod'] = 'Send using method';
$string['sendmethod_help'] = 'Send the notification via the methods, Like all available method (Email, popup, webpush) or send via only webpush';
$string['method_webpush'] = 'Only via Webpush';
$string['method_all'] = 'All available methods';
$string['manageevents'] = 'Manage Notifications';
$string['deleteconfirm'] = 'Are you sure you want to delete the selected notification from the webpush events?';
$string['duplicate'] = 'Duplicate notification';

// ...open window.
$string['directlink'] = 'Open the event';
$string['openwindow'] = 'Expand the content';

// PWA. String.
$string['pwa'] = 'PWA';
$string['pwaname'] = 'pwa name';
$string['pwaname_description'] = 'pwa name_description';
$string['pwathemecolor'] = 'pwathemecolor';
$string['pwathemecolor_description'] = 'pwathemecolor_description';
$string['pwabackgroundcolor'] = 'pwabackgroundcolor';
$string['pwabackgroundcolor_description'] = 'pwabackgroundcolor_description';
$string['pwaicons'] = 'pwaicons';
$string['pwaicons_desc'] = 'pwaicons_desc';
$string['lmsaceaddon'] = 'LMSACE Addon';
