<?php

namespace aceaddon_webpush;

use context_system;
use aceaddon_webpush\event;

require_once($CFG->libdir.'/formslib.php');
require_once(__DIR__.'/../lib.php');
require_once($CFG->dirroot.'/cohort/lib.php');

class event_form extends \moodleform {

    public function definition() {
        $mform = $this->_form;

        // Current edit item id, null for new items.
        $mform->addElement('hidden', 'id', 0);
        $mform->setType('id', PARAM_INT);

        // ... General section.
        $mform->addElement('header', 'general', get_string('general', 'aceaddon_webpush'));

        $mform->addElement('text', 'title', 'title');
        $mform->setType('title', PARAM_RAW);

        // Admin purpose only.
        $mform->addElement('textarea', 'description', 'Description');

        $list = aceaddon_webpush_eventslist();
        $events = $mform->addElement('autocomplete', 'event', 'Eventfor', $list);
        // $events->setMultiple(true);

        $options = [
            event::METHOD_WEBPUSH => get_string('method_webpush', 'aceaddon_webpush'),
            event::METHOD_ALL => get_string('method_all', 'aceaddon_webpush'),
        ];

        $mform->addElement('select', 'method', get_string('sendmethod', 'aceaddon_webpush'), $options);
        $mform->addHelpButton('method', 'sendmethod', 'aceaddon_webpush');

        // ...Notification receipents.

        $mform->addElement('header', 'recepients', get_string('recepients', 'aceaddon_webpush'));

        // Send to the user who are affected by this event.
        $mform->addElement('checkbox', 'relateduser', get_string('sendtorelateduser', 'aceaddon_webpush'));

        // Send to user who triggered the event.
        $mform->addElement('checkbox', 'eventuser', get_string('sendtoeventuser', 'aceaddon_webpush'));

        // Get list of roles in course and user context to get notifications.
        $roles = $this->get_roles(); // $instance->course_roles();
        // Roles of recipients that need ot receive notifications.
        $select = $mform->addElement('autocomplete', 'roles', get_string('roles', 'aceaddon_webpush'), $roles);
        $select->setMultiple(true);
        $mform->addHelpButton('roles', 'roles', 'aceaddon_webpush');

        $options = [
            event::ROLE_EVENTCONTEXT => get_string('role_eventcontext', 'aceaddon_webpush'),
            event::ROLE_SYSTEM_EVENTCONTEXT => get_string('role_system_eventcontext', 'aceaddon_webpush'),
            event::ROLE_ALL => get_string('role_all', 'aceaddon_webpush'),
        ];
        $mform->addElement('select', 'role_context', get_string('role_context', 'aceaddon_webpush'), $options);
        $mform->addHelpButton('role_context', 'role_context', 'aceaddon_webpush');

        // Cohorts based access.
        $cohorts = \cohort_get_all_cohorts();
        $cohortoptions = $cohorts['cohorts'];
        if ($cohortoptions) {
            array_walk($cohortoptions, function(&$value) {
                $value = $value->name;
            });
        }

        $cohort = $mform->addElement('autocomplete', 'cohorts', get_string('sendtocohorts', 'aceaddon_webpush'), $cohortoptions);
        $cohort->setMultiple(true);

        // Access rule by dates.
        $mform->addElement('header', 'notifiydateselector', get_string('notifiydateselector', 'aceaddon_webpush'));
        // Send the notifications to users once the data is reached.
        $mform->addElement('date_time_selector', 'start_date', get_string('from', 'aceaddon_webpush'), array('optional' => true));
        $mform->addHelpButton('start_date', 'from', 'aceaddon_webpush');
        // Stop sending notification to users until the end date reached.
        $mform->addElement('date_time_selector', 'end_date', get_string('until', 'aceaddon_webpush'), array('optional' => true));
        $mform->addHelpButton('end_date', 'until', 'aceaddon_webpush');

        // ...Notification content.

        $mform->addElement('header', 'notificationcontent', get_string('notificationcontent', 'aceaddon_webpush'));
        // Notification subject.
        $mform->addElement('text', 'subject', 'Subject');
        $mform->setType('subject', PARAM_TEXT);

        // Notification message.
        $mform->addElement('editor', 'message', 'Message');
        $this->message_placeholders($mform);

        $options = [
            event::LINKDIRECT => get_string('directlink', 'aceaddon_webpush'),
            event::LINKCONTENT => get_string('openwidow', 'aceaddon_webpush'),
        ];
        $mform->addElement('select', 'notificationlink', get_string('notificationlink', 'aceaddon_webpush'), $options);

        $this->add_action_buttons();
    }

    public function get_roles() {
        $roleoptions = role_get_names(\context_system::instance());
        $roles = [];
        foreach ($roleoptions as $role) {
            $roles[$role->id] = $role->localname;
        }
        return $roles;
    }

    /**
     * Add email placeholder fields in form fields.
     *
     * @param  \form $mform
     * @return void
     */
    public function message_placeholders(&$mform) {
        global $OUTPUT, $PAGE;

        $vars = \aceaddon_webpush\emailvars::vars();
        $templatecontext['messagevars'] = $vars;
        $mform->addElement('html', $OUTPUT->render_from_template('aceaddon_webpush/messagevars', $templatecontext));

        // Email tempalte placholders.
        $PAGE->requires->js_call_amd('mod_pulse/module', 'init');
    }
}
