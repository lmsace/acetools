<?php

namespace aceaddon_webpush;

class eventobserver {

    public static function config_log_created($event) {
        // print_object($eventdata);
        if ($event->other['plugin'] == 'aceaddon_webpush') {
            self::build_manifest();
        }
        // exit;
    }

    public static function build_manifest() {
        global $CFG, $SITE;

        /* print_object($SITE);
        exit; */
        $record = new \stdclass();
        $record->name = get_config('aceaddon_webpush', 'pwaname');
        $record->short_name = $SITE->shortname;
        $record->theme_color = get_config('aceaddon_webpush', 'pwathemecolor');
        $record->background_color = get_config('aceaddon_webpush', 'pwabackgroundcolor');
        $record->start_url = $CFG->wwwroot;
        $record->id = "moodle-lmsace-pwa-".$SITE->shortname;
        $record->display = "standalone";
        $record->scope = "./";

        $fs = get_file_storage();
        $contextid = \context_system::instance()->id;
        $files = $fs->get_area_files($contextid, 'aceaddon_webpush', 'pwaicons', false, '', false);

        $icons = [];
        $i = 0;
        foreach ($files as $file) {
            // print_object($file);
            $filename = $file->get_filename();
            $url = \moodle_url::make_pluginfile_url(
                $file->get_contextid(),
                $file->get_component(),
                $file->get_filearea(),
                $file->get_itemid(),
                $file->get_filepath(),
                $file->get_filename(),
                false
            );
            $imageinfo = $file->get_imageinfo();
            $filesize = $imageinfo['height'].'x'.$imageinfo['width'];
            $data = [
                "src" => $url->out(false),
                "type" => $file->get_mimetype(),
                "sizes" => $filesize,
            ];

            if ($i != 0) {
                $data['purpose'] = 'maskable';
            }

            $record->icons[] = $data;
            $i++;
        }
        // Prepare file record object
        $fileinfo = self::fileinfo();

        if ($file = self::get_manifestfile()) {
            // print_object($file);
            // Delete it if it exists
            $file->delete();
        }
        // Create file containing the json values of pwa.
        $result = $fs->create_file_from_string($fileinfo, str_replace('\\', '', json_encode($record)));
        /* print_object($result);
        exit;*/

    }

    public static function get_manifestfile() {
        $fs = get_file_storage();
        $fileinfo = self::fileinfo();
        // Get file
        $file = $fs->get_file($fileinfo['contextid'], $fileinfo['component'], $fileinfo['filearea'],
                $fileinfo['itemid'], $fileinfo['filepath'], $fileinfo['filename']);

        return $file;
    }

    public static function get_manifestfile_url() {

        $file = self::get_manifestfile();

        $url = \moodle_url::make_pluginfile_url(
            $file->get_contextid(),
            $file->get_component(),
            $file->get_filearea(),
            $file->get_itemid(),
            $file->get_filepath(),
            $file->get_filename(),
            false
        );

        return $url;
    }

    public static function fileinfo() {
        $contextid = \context_system::instance()->id;
        $fileinfo = array(
            'contextid' => $contextid, // ID of context
            'component' => 'aceaddon_webpush',     // usually = table name
            'filearea' => 'webmanifest',     // usually = table name
            'itemid' => 0,               // usually = ID of row in table
            'filepath' => '/',           // any path beginning and ending in /
            'filename' => 'app.webmanifest'); // any filename

        return $fileinfo;
    }


    public static function webpush_event_triggered($eventdata) {
        global $DB;

        \aceaddon_webpush\event::send_notification($eventdata);
        // $relateduser = $eventdata->relateduserid ?: $eventdata->userid;
        // exit;
    }
}
