<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Event controller for managing events and event items. Send event for all selected roles, cohorts.
 *
 * @package    aceaddon_webpush
 * @copyright  LMSACE DEV TEAM
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace aceaddon_webpush;


use custom_event;
use context_system;
use moodle_exception;
use core\navigation\views\primary;
use cache;
use cache_helper;
use context_coursecat;

class event {

    public $id;

    /**
     * Data of the custom event observer data by webpush.
     *
     * @var mixed
     */
    public $event;


    public const ROLE_EVENTCONTEXT = 1;

    public const ROLE_SYSTEM_EVENTCONTEXT = 2;

    public const ROLE_ALL = 3;

    public const METHOD_ALL = 2;

    public const METHOD_WEBPUSH = 1;

    public const LINKDIRECT = 1;

    public const LINKCONTENT = 2;

    public static function instance($event) {

        if (is_scalar($event)) {
            $event = self::get_event($event);
        }

        if (!is_array($event) && !is_object($event)) {
            throw new moodle_exception('eventformatnotcorrect', 'aceaddon_webpush');
        }
        return new self($event);
    }

    /**
     * Build event instance.
     *
     * @param int $id Record id of the event.
     */
    public function __construct($event) {
        global $DB;

        $this->id = $event->id;
        $this->event = $event;
    }

    /**
     * Find the event's visibility, it only consider the event own visiblity
     *
     * @return bool
     */
    public function is_visible() {
        global $DB;
        // Verify the event access rules for current user.
        return $this->event->visible ? true : false;
    }

    /**
     * Delete the current event.
     *
     * @return bool
     */
    public function delete_event() {
        global $DB;

        if ($DB->delete_records('aceaddon_webpush_events', ['id' => $this->id])) {
            return true;
        }

        return false;
    }



    /**
     * Duplicate events and its event items.
     *
     * @return void
     */
    public function duplicate() {
        global $DB;

        $record = $this->event;
        $record->id = 0;

        $duplicateid = self::manage_instance($record);

        \core\notification::success(get_string('eventduplicated', 'aceaddon_webpush'));

        return true;
    }

    /**
     * Update visibility.
     *
     * @param bool $visible
     * @return void
     */
    public function update_visible(bool $visible) {

        return $this->update_field('visible', $visible, ['id' => $this->id]);
    }

    /**
     * Update the field data in event record.
     *
     * @param string $key
     * @param mixed $value
     * @return bool
     */
    public function update_field($key, $value) {
        global $DB;
        $result = $DB->set_field('aceaddon_webpush_events', $key, $value, ['id' => $this->id]);

        return $result;
    }

    /**
     * Fetch the user for the event based on the event and custom event setup.
     * @param $eventdata Triggered event data.
     * @return $users REturns the moodle record_set of fetched event users.
     */
    public function get_userto($eventdata) {
        global $DB;

        $users = [];
        $params = [];
        $where = [];

        // is Event is configured to send related user. then add event related user in users list.
        if ($this->event->relateduser && $eventdata->relateduserid) {
            $users[] = $eventdata->relateduserid;
        }

        // Is Event is configured to send user who triggered the event. then add event triggered user in users list.
        if ($this->event->eventuser && $eventdata->userid) {
            $users[] = $eventdata->userid;
        }
        // Build query to add related user and event user to recive notification
        if (!empty($users)) {
            list($userinsql, $userinparams) = $DB->get_in_or_equal($users, SQL_PARAMS_NAMED, 'us');
            $where[] = "u.id $userinsql";
            $params += $userinparams;
        }

        // Include the selected roles users.
        $context = $eventdata->get_context();
        $contextid = $eventdata->get_context()->id;
        // Use the course context id for modules to fetch users, users are assigned using the course context id.
        if ($context->contextlevel == CONTEXT_MODULE) {
            $contextid = \context_course::instance($eventdata->courseid)->id;
        }

        $roles = $this->event->roles;
        if ($roles != '' && !empty($roles)) {

            list($insql, $inparam) = $DB->get_in_or_equal($roles, SQL_PARAMS_NAMED, 'rl');

            // Query for context based roles.
            switch ($this->event->role_context) {
                case event::ROLE_EVENTCONTEXT:
                    $contextsql = ' AND contextid=:eventcontext';
                    break;
                case event::ROLE_SYSTEM_EVENTCONTEXT:
                    $contextsql = ' AND (contextid=:systemcontext OR contextid=:eventcontext)';
                    break;
                default;
                    $contextsql = '';
            }

            $where[] = " u.id IN (SELECT userid FROM {role_assignments} WHERE roleid $insql $contextsql)";
            $params += [
                'systemcontext' => context_system::instance()->id,
                'eventcontext' => $contextid,
            ];
            $params = array_merge($params, $inparam);
        }

        // Include the cohort users.
        $cohorts = $this->event->cohorts;
        if ($cohorts != '' && !empty($cohorts)) {

            list($insql, $inparam) = $DB->get_in_or_equal($cohorts, SQL_PARAMS_NAMED, 'ch');
            $where[] = " u.id IN (
                SELECT * FROM {cohort_members} cm
                JOIN {cohort} c ON cm.cohortid = c.id
                WHERE c.id $insql ) ";

            $params += $inparam;
        }

        if (empty($where)) {
            return false;
        }

        $sql = "SELECT u.*  FROM {user} u";
        $sql .= (!empty($where)) ? ' WHERE '.implode(' OR ', $where) : '';
        $sql .= ' GROUP BY u.id';

        $users = $DB->get_records_sql($sql, $params);

        return $users; // RECORD SET.
    }


    /**
     * Restrict the event notification by date.
     *
     * @param  mixed $eventdata
     * @return void
     */
    public function restriction_bydate($eventdata) {

        $startdate = $this->event->start_date;
        $enddate = $this->event->end_date;
        // Check any of the start date or end date is configured.
        if (empty($startdate) && empty($enddate)) {
            return true;
        }

        $date = new \DateTime("now", \core_date::get_user_timezone_object());
        $today = $date->getTimestamp();

        // Verify the started date is reached.
        if (!empty($startdate) && $startdate > $today) {
            return false;
        }

        // If the notification startdate reached or no start date, then check the enddate is reached.
        if (!empty($enddate) && $enddate < $today) {
            return false;
        }
        // Event is configured between the start and end date.
        return true;
    }

    /**
     * Build the notification content.
     * @param stdclass $eventdata  This is the data of moodle event data.
     */
    public function build($eventdata) {
        global $CFG, $USER;

        if (empty($eventdata)) {
            return false;
        }

        // Check the restrictions.
        if (!$this->restriction_bydate($eventdata)) {
            return false;
        }

        $currentuser = $USER; // Current user.
        $oldlanguage = current_language(); // Save the current lanauge.

        $templatetext = $this->event->message ? $this->event->message['text'] : $eventdata->get_description();
        $subject = $this->event->subject ? format_string($this->event->subject) : $eventdata->get_name();

        // Generaate coorect users to receive notificatoin based on settings.
        $users = $this->get_userto($eventdata);

        if (empty($users)) {
            return  false;
        }

        // General data about the event message, common for all users. Used as final data for send messages.
        $data = new \core\message\message();
        $data->name = 'aceaddon_webpush';
        $data->component = 'aceaddon_webpush';
        $data->courseid = $eventdata->courseid ?: SITEID;
        $course = get_course($data->courseid);
        // $eventdata->modulename = 'pulse';
        // TODO: NEED to use dynamic sender.
        $data->userfrom = $sender = \core_user::get_support_user();
        $data->contexturl = $eventdata->get_url();
        $context = $eventdata->get_context();
        $filearea = 'webpush_eventmessage';

        // Event related data for email vars.
        // Event triggered user.
        $eventuser = \core_user::get_user($eventdata->userid);
        // Event affected user.
        $relateduser = new \stdclass();
        if ($eventdata->relateduserid) {
            $relateduser = \core_user::get_user($eventdata->relateduserid);
        }
        $event = (object)  $eventdata->get_data();
        $event->name = $eventdata->get_name();
        $event->description = $eventdata->get_description();
        $event->instance = $this->get_event_instance($eventdata);
        $event->title = $this->event->title;

        $list = [];

        foreach ($users as $userid => $userto) {

            $messagedata = clone $data;
            // If this user is already include in the list then continue to next user.
            if (in_array($userid, array_keys($list))) {
                continue;
            }
            // Setup the current user as session user and force the user language as the current language.
            force_current_language($userto->lang);
            \core\session\manager::set_user($userto);

            // Replace the email text placeholders with data.
            // Update the notificaiton content placeholders with event data.
            list($subject, $messagehtml) = self::update_notification_vars(
                $templatetext, $subject, $userto, $eventuser, $relateduser, $course, $sender, $event
            );

            // Rewrite the plugin file placeholders in the email text.
            $messagehtml = file_rewrite_pluginfile_urls($messagehtml, 'pluginfile.php',
                $context->id, 'aceaddon_webpush', $filearea, 0);

            // Format filter supports. filter the enabled filters.
            $messagehtml = format_text($messagehtml);
            $messageplain = html_to_text($messagehtml); // Plain text.

            $messagedata->userto = $userto;
            $messagedata->subject = format_string($subject);
            $messagedata->fullmessage = $messageplain;
            $messagedata->fullmessageformat = FORMAT_HTML;
            $messagedata->fullmessagehtml = $messagehtml;
            $messagedata->smallmessage = $subject;
            // Include the user in list to send notification.

            $list[$userto->id] = $messagedata;
        }
        // Return to curent user.
        \core\session\manager::set_user($currentuser);
        // Force back to current lanauge.
        force_current_language($oldlanguage);

        return $list;
    }

    public function get_event_instance($eventdata) {
        global $DB;

        $contextlevel = $eventdata->contextlevel;
        $contextinstanceid = $eventdata->contextinstanceid;

        switch ($contextlevel) {

            case CONTEXT_COURSE:
                $instance = get_course($contextinstanceid);
                if (!empty($instance)) {
                    $instance->url = new \moodle_url('/course/view.php', ['id' => $contextinstanceid]);
                }
                break;

            case CONTEXT_MODULE:
                $instance = $DB->get_record_sql('
                    SELECT cm.*, m.name as modname FROM {course_modules} cm
                    JOIN {modules} m ON m.id = cm.module
                    WHERE cm.id=:id',
                    array('id' => $contextinstanceid)
                );

                $instance = get_coursemodule_from_id($instance->modname, $contextinstanceid);

                if (!empty($instance)) {
                    $instance->url = new \moodle_url('/mod/'.$instance->modname.'/view.php', ['id' => $contextinstanceid]);
                }
                break;

            // case CONTEXT_COURSECAT:

        }

        return isset($instance) ? $instance : new \stdclass();
    }

    /**
     * Insert or update the event instance to DB. Convert the multiple options select elements to json.
     * setup event path after insert/update.
     *
     * @param stdclass $formdata
     * @return bool
     */
    public static function manage_instance($formdata) {
        global $DB;

        $record = $formdata;

        $record->message_format = $formdata->message['format'];
        $record->message = $formdata->message['text'];

        $record->roles = json_encode($formdata->roles);

        /* $record->location = json_encode($formdata->location);*/

        $record->cohorts = json_encode($formdata->cohorts);
        // $record->events = $formdata->event;

		// set_config('webpush_events', $formdata->events, 'aceaddon_webpush');
         $transaction = $DB->start_delegated_transaction();

        if (isset($formdata->id) && $DB->record_exists('aceaddon_webpush_events', ['id' => $formdata->id])) {
            $eventid = $formdata->id;

            $DB->update_record('aceaddon_webpush_events', $record);
            // Show the edited success notification.
            // \core\notification::success(get_string('updatesuccess', 'aceaddon_webpush'));
        } else {
            // Setup the event order.
            $eventid = $DB->insert_record('aceaddon_webpush_events', $record);
            // Show the event inserted success notification.
            // \core\notification::success(get_string('insertsuccess', 'aceaddon_webpush'));
        }

        // Remove the event observers and recreate.
        $cache = \cache::make('core', 'observers');
        $cache->delete('all');

        // bUILD THE OBSERVERS AGAIN
        $list = \core\event\manager::get_all_observers();
        $transaction->allow_commit();

        return true;
    }

    /**
     * Get the event.
     *
     * @param [type] $id
     * @return void
     */
    public static function get_event($eventid) {
        global $DB;

        // Verfiy and Fetch event record from DB.
        if ($record = $DB->get_record('aceaddon_webpush_events', ['id' => $eventid])) {
            $record->message = [
                'text'   => $record->message,
                'format' => $record->message_format
            ];
            // Decode the multiple option select elements values to array.
            $record->roles = json_decode($record->roles);
            $record->cohorts = json_decode($record->cohorts);

            return $record;
        } else {
            // TODO: string for event not found.
            throw new moodle_exception('eventnotfound', 'aceaddon_webpush');
        }
        return false;
    }

    /**
     * Get count of available events.
     *
     * @return int
     */
    public static function get_eventscount() {
        global $DB;
        return $DB->count_records('aceaddon_webpush_events', []);
    }


    /**
     * Undocumented function
     *
     * @return object
     */
    public static function get_events() {
        global $DB;
        $topevents = $DB->get_records('aceaddon_webpush_events', [], 'id ASC');
        return $topevents;
    }

    public static function get_events_fromname($eventname) {
        global $DB;
        $events = $DB->get_records('aceaddon_webpush_events', ['event' => $eventname], 'id ASC');
        return $events;
    }

    /**
     * Send the notification.
     */
    public static function send_notification($eventdata) {
		global $DB;
		$eventname = $eventdata->eventname;

		$events = self::get_events_fromname($eventname);
		foreach ($events as $event) {
			$event = self::instance($event->id);
			$usersdata = $event->build($eventdata);
            // print_object($usersdata);exit;
			if (!empty($usersdata)) {
				// print_object($usersdata);
				foreach ($usersdata as $userid => $data) {
                    if ($event->event->method == self::METHOD_ALL) {
                        message_send($data);
                    } else {
                        // echo "send notification -webpush";
                        \aceaddon_webpush\webpushserver::send_event_notification($data);
                    }
				}
			}
		}
	}

    public static function update_notification_vars($templatetext, $subject, $userto, $eventuser, $relateduser, $course, $sender, $event) {
        global $DB, $CFG;

        $sender = $sender ? $sender : \core_user::get_support_user(); // Support user.
        $amethods = \aceaddon_webpush\emailvars::vars(); // List of available placeholders.
        $vars = new \aceaddon_webpush\emailvars($userto, $eventuser, $relateduser, $course, $sender, $event);

        foreach ($amethods as $funcname) {
            $replacement = "{" . $funcname . "}";
            // Message text placeholder update.

            if (stripos($templatetext, $replacement) !== false) {
                $val = $vars->$funcname;
                // Placeholder found on the text, then replace with data.
                $templatetext = str_ireplace($replacement, $val, $templatetext);
                // print_object($templatetext);
            }
            // Replace message subject placeholder.
            if (stripos($subject, $replacement) !== false) {
                $val = $vars->$funcname;
                $subject = str_ireplace($replacement, $val, $subject);
            }
        }
        // exit;
        return [$subject, $templatetext];
    }


}