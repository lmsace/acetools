<?php

namespace aceaddon_webpush;

require_once $CFG->dirroot.'/local/acetools/addons/webpush/lib/autoloader.php';


use SKien\PNServer\PNSubscription;
use SKien\PNServer\PNPayload;
use SKien\PNServer\PNServer;

class webpushserver {

	function __construct() {

	}

	/**
     * Renders the event list page with filter form and datatable.
     *
     * @param eventfilter_form $form Event filter form.
     * @param array $tabledata An array of event data to be used by the datatable.
     * @return string HTML to be displayed.
     */
    public function render_event_list($form, $tabledata) {
		global $PAGE;

        $title = get_string('pluginname', 'report_eventlist');

		$html = '';
        // Form.
        ob_start();
        $form->display();
        $html .= ob_get_contents();
        ob_end_clean();

        $PAGE->requires->yui_module('moodle-report_eventlist-eventfilter', 'Y.M.report_eventlist.EventFilter.init',
                array(array('tabledata' => $tabledata)));
        $PAGE->requires->strings_for_js(array(
            'eventname',
            'component',
            'action',
            'crud',
            'edulevel',
            'affectedtable',
            'dname',
            'legacyevent',
            'since'
            ), 'report_eventlist');
        $html .= \html_writer::start_div('report-eventlist-data-table', array('id' => 'report-eventlist-table'));
        $html .= \html_writer::end_div();

        return $html;
    }

	/* public function generate_notificationdata($eventdata) {

		// $record = new stdclass();
	}

	public function send_webpush($eventdata) {
		$this->generate_notificationdata($eventdata);
	} */



	public static function getmyvapid() {
		global $CFG;

		$mailto = get_config('aceaddon_webpush', 'mailto');
		$publickey = get_config('aceaddon_webpush', 'publickey');
		$privatekey = get_config('aceaddon_webpush', 'privatekey');

		/**
		 * set the generated VAPID key and rename to MyVapid.php
		 *
		 * you can generate your own VAPID key on https://tools.reactpwa.com/vapid.
		 */
		 $oVapid = new \SKien\PNServer\PNVapid(
			"mailto: $mailto",
			$publickey,
			$privatekey
		 );
		return $oVapid;
	}


	public static function send_event_notification($data) {
		global $DB, $CFG, $OUTPUT, $SITE;

		// require_once $CFG->dirroot.'/local/webpush/lib.php';

		$icon = $OUTPUT->get_logo_url();
		// $webPush = new WebPush($auth);

		if ($record = $DB->get_record('aceaddon_webpush_subs', array('userid' => $data->userto->id))) {
			
			// print_object($data->userto);
			// $subscription = Subscription::create((array) json_decode($record->subscription_json));
			$oServer = new PNServer(null);
			// $oServer->setLogger($logger);

			// Set our VAPID keys
			$oServer->setVapid(self::getmyvapid());

			// Subject as title;
			$title = $data->subject;
			// Event message or description as message content.
			$content = strip_tags($data->fullmessage);
			// echo $content;
			// $content = $data['fullmessage'];
			$viewurl = $data->contexturl;

			// Create and set payload
			// - we don't set a title - so service worker uses default
			// - URL to icon can be
			//    * relative to the origin location of the service worker
			//    * absolute from the homepage (begining with a '/')
			//    * complete URL (beginning with https://)
			$oPayload = new PNPayload($title, $content, $icon);
			$oPayload->setTag('news', true);
			$oPayload->setURL($viewurl);

			$oServer->setPayload($oPayload);

			// load subscriptions from database
			$oServer->addSubscription(PNSubscription::fromJSON((string) $record->subscription_json));

			// ... and finally push !
			// print_object('pushed');
			

			
			if (!$oServer->push()) {
				\core\notification::error( $oServer->getError() );
			}
		}
	}

	public static function verify_libraries() {
		// List of libraries used in webpush PNserver.
		$libraries = ['curl', 'mbstring', 'openssl', 'gmp', 'bcmath'];
		$missing = [] ;
		foreach ($libraries as $library) {
			if (!extension_loaded($library)) {
				$missing[] = $library;
			}
		}

		return (count($missing) > 0) ? $missing : true;
	}
}
