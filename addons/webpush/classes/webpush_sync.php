<?php

namespace aceaddon_webpush;

class webpush_sync extends \local_acetools\tools_wrapper {

    // Method to run on the page init called form the lmsace themes not supported for other themes.
    public static function page_init($page) {

    }

    public static function settings_navigation() {
        global $PAGE, $DB, $USER;

        if (\aceaddon_webpush\webpushserver::verify_libraries() === true) {

            // Public key.
            $publickey = get_config('aceaddon_webpush', 'publickey');
            $data = [
                'initservice' => isloggedin(), 'publickey' => $publickey, 'context' => \context_system::instance()->id
            ];

            // Include data for JS.
            $PAGE->requires->data_for_js('webpush', $data, true);
            // Init the push subscription js to register service works and push and subscription api.
            $PAGE->requires->js_call_amd('aceaddon_webpush/push-subscription', 'init', [$data]);

            /* $data['userto'] = $USER;
            $data['subject'] = 'TEst';
            $data['contexturl'] = new \moodle_url('\p');
            \aceaddon_webpush\webpushserver::send_event_notification((object) $data);  */
        }
    }


}
