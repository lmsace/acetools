<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Table to list the events.
 * 
 * @package    aceaddon_webpush
 * @copyright  LMSACE Dev Team
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace aceaddon_webpush\table;

require_once($CFG->dirroot.'/lib/tablelib.php');
use core_table\dynamic as dynamic_table;
use moodle_url;
use html_writer;
use \aceaddon_webpush\event as event;

/**
 * List of Events table.
 */
class events extends \table_sql {

    /**
     * Setup and Render the events table.
     *
     * @param int $pagesize Size of page for paginated displayed table.
     * @param bool $useinitialsbar Whether to use the initials bar which will only be used if there is a fullname column defined.
     * @param string $downloadhelpbutton
     */
    public function out($pagesize, $useinitialsbar, $downloadhelpbutton = '') {

        // Define table headers and columns.
        $columns = ['title', 'description', 'event', 'roles',  'restrictions', 'action'];
        $headers = [
            get_string('title', 'aceaddon_webpush'),
            get_string('description', 'aceaddon_webpush'),
            get_string('event', 'aceaddon_webpush'),
            get_string('roles', 'aceaddon_webpush'),
            // get_string('cohorts', 'aceaddon_webpush'),
            get_string('restrictions', 'aceaddon_webpush'),           
            get_string('action'),
        ];

        $this->define_columns($columns);
        $this->define_headers($headers);

        $this->sortable(false);
        $this->guess_base_url();

        parent::out($pagesize, $useinitialsbar, $downloadhelpbutton);
    }

    /**
     * Guess the base url for the menu items table.
     */
    public function guess_base_url(): void {
        $this->baseurl = new moodle_url('/local/acetools/addons/webpush/events.php');
    }

    /**
     * Set the sql query to fetch events list.
     *
     * @param int $pagesize Size of page for paginated displayed table.
     * @param boolean $useinitialsbar Whether to use the initials bar which will only be used if there is a fullname column defined.
     * @return void
     */
    public function query_db($pagesize, $useinitialsbar = true) {
        // Fetch all avialable records from event table.
        $this->set_sql('*', '{aceaddon_webpush_events}', '1=1');

        parent::query_db($pagesize, $useinitialsbar);
    }

    /**
     * Show the event title in the list, 
     *
     * @param object $row
     * @return void
     */
    public function col_title($row) {
        global $OUTPUT;
        
        $title = html_writer::tag('h6', $row->title, ['class' => 'event-title']);        
        return $title;
    }

    public function col_description($row) {

        $description = format_text($row->description, FORMAT_HTML);
        return $description;
    }
    
    public function col_roles($row) {
        global $DB;

        $roles = ($row->roles);

        if ($row->roles != '' && !empty(json_decode($row->roles))) {
            $roles = json_decode($row->roles);
            $rolelist = $DB->get_records_list('role', 'id', $roles);
            $rolenames = role_fix_names($rolelist);
            array_walk($rolenames, function(&$value) {
                $value = html_writer::tag('span', $value->localname, ['class' => 'badge badge-primary']);
            });

            return implode(' ', $rolenames);
        }

        return '-';
    }

    /**
     * Display the access rules configured in menu. Fetch the user readable names for roles, cohorts, lanauges and dates.
     *
     * @param  object $row
     * @return string $html
     */
    public function col_restrictions($row) {
        global $DB;

        $rules = [];

        if ($row->start_date) {
            $rules[] = [
                'name' => get_string('from', 'aceaddon_webpush'),
                'value' => userdate($row->start_date, get_string('strftimedate', 'core_langconfig') )
            ];

        }
        if ($row->end_date) {
            $rules[] = [
                'name' => get_string('until', 'aceaddon_webpush'),
                'value' => userdate($row->end_date, get_string('strftimedate', 'core_langconfig') )
            ];

        }

        $html = '';
        foreach ($rules as $rule) {
            $html .= html_writer::tag('li', html_writer::tag('label', $rule['name']) . $rule['value']);
        }
        return $html ? html_writer::tag('ul', $html) : '-';
    }
  
    public function col_cohorts($row) {
        global $DB;

        if ($row->cohorts != '' && !empty(json_decode($row->cohorts))) {
            $cohorts = json_decode($row->cohorts);
            $cohortlist = $DB->get_records_list('cohort', 'id', $cohorts);

            array_walk($cohortlist, function(&$value) {
                $value = html_writer::tag('span', $value->name, ['class' => 'badge badge-primary']);
            });
            return implode(' ', $cohortlist);
        }
        return '-';
    }

   
    public function col_action($row) {
        global $OUTPUT, $PAGE;

        // Current visible status
        $status = $row->visible ? get_string('enable') : get_string('disable');
        $badge = $row->visible ? 'badge-success' : 'badge-danger';
        $statusbadge = html_writer::tag('span', $status, ['class' => 'mr-1 badge '.$badge]);

        $baseurl = new \moodle_url('/local/acetools/addons/webpush/events.php', [
            'id' => $row->id,
            'sesskey' => \sesskey()
        ]);
        $actions = array();
        // Edit.
        $actions[] = array(
            'url' => new moodle_url('/local/acetools/addons/webpush/edit_events.php', [
                'id' => $row->id,
                'sesskey' => sesskey()
            ]),
            'icon' => new \pix_icon('t/edit', \get_string('edit')),
            'attributes' => array('class' => 'action-edit')
        );

        // Make the event duplicate.
        $actions[] = array(
            'url' => new \moodle_url($baseurl, ['action' => 'copy']),
            'icon' => new \pix_icon('t/copy', \get_string('duplicate', 'aceaddon_webpush')),
            'attributes' => array('class' => 'action-copy')
        );

        
        // Delete.
        $actions[] = array(
            'url' => new \moodle_url($baseurl, array('action' => 'delete')),
            'icon' => new \pix_icon('t/delete', \get_string('delete')),
            'attributes' => array('class' => 'action-delete'),
            'action' => new \confirm_action(get_string('deleteconfirm', 'aceaddon_webpush'))
        );


        // Show/Hide.
        if ($row->visible) {
            $actions[] = array(
                'url' => new \moodle_url($baseurl, array('action' => 'hideevent')),
                'icon' => new \pix_icon('t/hide', \get_string('hide')),
                'attributes' => array('data-action' => 'hide', 'class' => 'action-hide')
            );
        } else {
            $actions[] = array(
                'url' => new \moodle_url($baseurl, array('action' => 'showevent')),
                'icon' => new \pix_icon('t/show', \get_string('show')),
                'attributes' => array('data-action' => 'show', 'class' => 'action-show')
            );
        }


        $actionshtml = array();
        foreach ($actions as $action) {
            $action['attributes']['role'] = 'button';
            $actionshtml[] = $OUTPUT->action_icon($action['url'], $action['icon'], ($action['action'] ?? null), $action['attributes']);
        }
        return $statusbadge . html_writer::span(join('', $actionshtml), 'menu-item-actions item-actions mr-0');
    }
}
