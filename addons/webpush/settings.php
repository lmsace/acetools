<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package   theme_boost
 * @copyright 2016 Ryan Wyllie
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();


if ($hassiteconfig) {

  /*   core\event
config_log_created */
// exit;
    global $CFG;

    require_once($CFG->dirroot.'/local/acetools/addons/webpush/lib.php');

    $settings = new theme_boost_admin_settingspage_tabs('aceaddon_webpush', get_string('pluginname', 'aceaddon_webpush'));

    $settingspage = new admin_settingpage('managelocalwebpush', new lang_string('pluginname', 'aceaddon_webpush'));

    if ($ADMIN->fulltree) {

        $name = 'aceaddon_webpush/mailto';
        $title = get_string('mailto', 'aceaddon_webpush');
        $description = get_string('mailto_description', 'aceaddon_webpush');
        $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_RAW);
        $settingspage->add($setting);

        $name = 'aceaddon_webpush/publickey';
        $title = get_string('publickey', 'aceaddon_webpush');
        $description = get_string('publickey_description', 'aceaddon_webpush');
        $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_RAW);
        $settingspage->add($setting);

        $name = 'aceaddon_webpush/privatekey';
        $title = get_string('privatekey', 'aceaddon_webpush');
        $description = get_string('privatekey_description', 'aceaddon_webpush');
        $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_RAW);
        $settingspage->add($setting);

        $libraries = \aceaddon_webpush\webpushserver::verify_libraries();
        if ($libraries !== true) {
            $setting = new admin_setting_description("librarymissing", "", get_string('missinglibraries', 'aceaddon_webpush', implode(',', $libraries)) );
            $settingspage->add($setting);
        }
    }

    $settings->add($settingspage);

    $page = new admin_settingpage('managepwa', new lang_string('pwa', 'aceaddon_webpush'));

    $name = 'aceaddon_webpush/pwaname';
    $title = get_string('pwaname', 'aceaddon_webpush');
    $description = get_string('pwaname_description', 'aceaddon_webpush');
    $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_RAW);
    $page->add($setting);

    $name = 'aceaddon_webpush/pwathemecolor';
    $title = get_string('pwathemecolor', 'aceaddon_webpush');
    $description = get_string('pwathemecolor_description', 'aceaddon_webpush');
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '');
    $page->add($setting);

    $name = 'aceaddon_webpush/pwabackgroundcolor';
    $title = get_string('pwabackgroundcolor', 'aceaddon_webpush');
    $description = get_string('pwabackgroundcolor_description', 'aceaddon_webpush');
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '');
    $page->add($setting);

    $name = 'aceaddon_webpush/pwaicons';
    $title = get_string('pwaicons', 'aceaddon_webpush');
    $description = get_string('pwaicons_desc', 'aceaddon_webpush');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'pwaicons', 0, [
        'maxfiles' => -1,
        'accepted_types' => 'web_image'
    ]);
    $page->add($setting);

    $settings->add($page);

    $ADMIN->add('localacetools', $settings);

    $manageevents = new \moodle_url('/local/acetools/addons/webpush/events.php');
    $ADMIN->add(
        'localacetools',
        new admin_externalpage('aceaddon_webpush_events', get_string('eventslist', 'aceaddon_webpush'), $manageevents)
    );
}
