<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * List the available events and manage the event Create, Update, Delete actions, sort the order of events.
 *
 * @package    aceaddon_webpush
 * @copyright  LMSACE Dev team.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// Require config.
require(__DIR__.'/../../../../config.php');

// Require admin library.
require_once($CFG->libdir.'/adminlib.php');

$id = optional_param('id', null, PARAM_INT);
$action = optional_param('action', null, PARAM_ALPHAEXT);

// Page values.
$url = new moodle_url('/local/acetools/addons/webpush/events.php');
$context = \context_system::instance();

// Extend the features of admin settings.
admin_externalpage_setup('aceaddon_webpush_events');

// Setup page values.
$PAGE->set_url($url);
$PAGE->set_context($context);
$PAGE->set_heading(get_string('manageevents', 'aceaddon_webpush'));
// Setup the breadcrums for qick access.
$PAGE->navbar->add(get_string('themes', 'core'), new moodle_url('/admin/category.php', array('category' => 'themes')));
$PAGE->navbar->add(get_string('pluginname', 'aceaddon_webpush'), new moodle_url('/admin/category.php',
    array('category' => 'aceaddon_webpush'))
);
$PAGE->navbar->add(get_string('events', 'aceaddon_webpush'), new moodle_url('/local/acetools/addons/webpush/events.php'));

// Verfiy the user session.
if ($action !== null && confirm_sesskey())  {
    // Every action is based on event, Event id param should exist.
    $id = required_param('id', PARAM_INT);
    // Create event instance. Actions are performed in event instance.
    $event = aceaddon_webpush\event::instance($id);

    $transaction = $DB->start_delegated_transaction();
    // Perform the requested action.
    switch ($action) :
        // Triggered action is delete, then init the deletion of event.
        case 'delete':
            // Delete the event.
            if ($event->delete_event()) {
                // Notification to user for event deleted success.
                \core\notification::success(get_string('eventdeleted', 'aceaddon_webpush'));
            }
            break;
        case "copy":
            // Duplicate the events.
            $event->duplicate();
            break;
        case "hideevent":
            // Disable the event to send notificaiotn to users.
            $event->update_visible(false);
            break;
        case "showevent":
            // Enable the event to send notificaiotn to users.
            $event->update_visible(true);
            break;
    endswitch;

    // Allow to update the changes to database.
    $transaction->allow_commit();
    // End of any action redirect to overview page for clear the params from url.
    redirect($url);
}

// Page content display started.
echo $OUTPUT->header();

// Event heading.
echo $OUTPUT->heading(get_string('events', 'aceaddon_webpush'));

// Event description.
echo $OUTPUT->box(get_string('events_desc', 'aceaddon_webpush'));

$addnew = new moodle_url('/local/acetools/addons/webpush/edit_events.php');
// $link = html_writer::($addnew, );
$link = new single_button($addnew, get_string('addnewevent', 'aceaddon_webpush'), 'GET', true);
echo html_writer::div($OUTPUT->render($link), 'text-right d-block mb-2');
// Events list table.
$table = new \aceaddon_webpush\table\events($context->id);
$table->out(10, true);

// Footer.
echo $OUTPUT->footer();
