<?php

$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

if ($contentType === "application/json") {
	  //Receive the RAW post data.
 	$content = trim(file_get_contents("php://input"));

	// $data = json_decode($content, true);

	// $sub = json_encode($data['subscription'], true);

	// print_r($sub);exit;
	// If json_decode failed, the JSON is invalid.
	if( !empty($content) ) {

		require_once('../../config.php');

		global $USER;

		if ($record = $DB->get_record('aceaddon_webpush_subs', array('userid' => $USER->id ) )) {
			$record->subscription_json = $content; //json_encode($content);
			$record->timecreated = time();
			$DB->update_record('aceaddon_webpush_subs', $record);

		} else {
			$DB->insert_record('aceaddon_webpush_subs', array(
				'userid' => $USER->id,
				'subscription_json' => $content,// json_encode($data['subscription']),
				'timecreated' => time()
			));
		}

	} else {
	    // Send error back to user.
	}
}
