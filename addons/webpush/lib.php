<?php

function aceaddon_webpush_output_fragment_store_subscription($args) {
	global $USER, $DB;

	if ($record = $DB->get_record('aceaddon_webpush_subs', array('userid' => $USER->id ) )) {
		$record->subscription_json = $args['subscription'];
		$record->timecreated = time();
		$DB->update_record('aceaddon_webpush_subs', $record);

	} else {
		$DB->insert_record('aceaddon_webpush_subs', array(
			'userid' => $USER->id,
			'subscription_json' => $args['subscription'], // json_encode($data['subscription']),
			'timecreated' => time()
		));
	}
}


/**
 * Serve the files from the Pulse file areas
 *
 * @param stdClass $course the course object
 * @param stdClass $cm the course module object
 * @param stdClass $context the context
 * @param string $filearea the name of the file area
 * @param array $args extra arguments (itemid, path)
 * @param bool $forcedownload whether or not force download
 * @param array $options additional options affecting the file serving
 * @return bool false if the file not found, just send the file otherwise and do not return anything
 */
function aceaddon_webpush_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()) {
    // Check the contextlevel is as expected - if your plugin is a block, this becomes CONTEXT_BLOCK, etc.
    if ($context->contextlevel != CONTEXT_MODULE && $context->contextlevel != CONTEXT_SYSTEM) {
        return false;
    }
    // Get extended plugins files.
    $files = ['pwaicons', 'pwasplashscreens', 'webmanifest'];
    // Make sure the filearea is one of those used by the plugin.
    if (!in_array($filearea, $files)) {
        return false;
    }

    // Item id is 0.
    $itemid = array_shift($args);

    // Extract the filename / filepath from the $args array.
    $filename = array_pop($args); // The last item in the $args array.
    if (!$args) {
        $filepath = '/'; // ...$args is empty => the path is '/'
    } else {
        $filepath = '/'.implode('/', $args).'/'; // ...$args contains elements of the filepath
    }

    // Retrieve the file from the Files API.
    $fs = get_file_storage();
    $file = $fs->get_file($context->id, 'aceaddon_webpush', $filearea, $itemid, $filepath, $filename);
    if (!$file) {
        return false; // The file does not exist.
    }

    // We can now send the file back to the browser - in this case with a cache lifetime of 1 day and no filtering.
    send_stored_file($file, 86400, 0, $forcedownload, $options);
}


function aceaddon_webpush_eventslist() {

	$completelist = report_eventlist_list_generator::get_all_events_list();
	$list = [];
	foreach ($completelist as $key => $event) {
		$list[$key] = $event['raweventname'];
	}
	return $list;
}

/**
 * 
 * @return array
 */
function aceaddon_webpush_get_observe_eventslist(): array {
	$data = 
	$events = aceaddon_webpush\event::get_events();
	$list = [];
	
	foreach ($events as $id => $event) {
		$list[] = [
			'eventname' => $event->event,
        	'callback' => '\aceaddon_webpush\eventobserver::webpush_event_triggered'
		];
	}
	return $list ?? [];
}
