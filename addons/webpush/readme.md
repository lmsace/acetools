Version 1.0 Free
-----------------

Only send the moodle notification as webpush.

Pro
----

Send the custom event notification as webpush
Send a custom event notification
Options to set button links.
click handler, open the link or open in tab with custom full message.
Send custom message instantly.

PWA Support
----------